<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => 'c6211569d5625431cc0d4fa624f0686f81311e4f',
    'name' => 'liquidweb/nexcess-mapps',
  ),
  'versions' => 
  array (
    'composer/installers' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a0357fccad9d1cc1ea0c9a05b8847fbccccb78d',
    ),
    'cweagans/composer-patches' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ae02121445ad75f4eaff800cc532b5e6233e2ddf',
    ),
    'liquidweb/nexcess-mapps' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'c6211569d5625431cc0d4fa624f0686f81311e4f',
    ),
    'liquidweb/nexcess-mapps-dashboard' => 
    array (
      'pretty_version' => 'v1.3.6',
      'version' => '1.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '621dd30112723b59ba64cd940921f930788a1c70',
    ),
    'liquidweb/woocommerce-upper-limits' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '594dc658c309572a5968d5481b2ff974f0e1ba4e',
    ),
    'roundcube/plugin-installer' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'shama/baton' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'stevegrunwell/wp-admin-tabbed-settings-pages' => 
    array (
      'pretty_version' => 'v0.2.0',
      'version' => '0.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd8fbcce4bde4fbee623c659166b42bce0a7d09d8',
    ),
    'stevegrunwell/wp-cache-remember' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f37b8e79d9cb05f5fd0e8035b0ffecbb1ded182e',
    ),
    'wp-cli/wp-config-transformer' => 
    array (
      'pretty_version' => 'v1.2.8',
      'version' => '1.2.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bb2b9162c38ca72370380aea11dc06e431e13a5',
    ),
    'wpackagist-plugin/display-environment-type' => 
    array (
      'pretty_version' => '1.3',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/1.3',
    ),
    'wpackagist-plugin/varnish-http-purge' => 
    array (
      'pretty_version' => '4.8.1',
      'version' => '4.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/4.8.1',
    ),
    'wpackagist-plugin/wp-fail2ban' => 
    array (
      'pretty_version' => '4.3.0.9',
      'version' => '4.3.0.9',
      'aliases' => 
      array (
      ),
      'reference' => 'tags/4.3.0.9',
    ),
  ),
);
