<?php

/**
 * Perform regular maintenance.
 */

namespace Nexcess\MAPPS\Integrations;

use Nexcess\MAPPS\Concerns\HasCronEvents;
use Nexcess\MAPPS\Services\MigrationCleaner;

class Maintenance extends Integration {
	use HasCronEvents;

	/**
	 * The MigrationCleaner instance.
	 *
	 * @var \Nexcess\MAPPS\Services\MigrationCleaner
	 */
	protected $migrationCleaner;

	const DAILY_MAINTENANCE_CRON_ACTION  = 'nexcess_mapps_daily_maintenance';
	const WEEKLY_MAINTENANCE_CRON_ACTION = 'nexcess_mapps_weekly_maintenance';

	/**
	 * @param \Nexcess\MAPPS\Services\MigrationCleaner $cleaner
	 *
	 * @return self
	 */
	public function __construct( MigrationCleaner $cleaner ) {
		$this->migrationCleaner = $cleaner;
	}

	/**
	 * Perform any necessary setup for the integration.
	 *
	 * This method is automatically called as part of Plugin::loadIntegration(), and is the
	 * entry-point for all integrations.
	 */
	public function setup() {
		$this->registerCronEvent( self::DAILY_MAINTENANCE_CRON_ACTION, 'daily' );
		$this->registerCronEvent( self::WEEKLY_MAINTENANCE_CRON_ACTION, 'weekly' );
		$this->addHooks();
	}

	/**
	 * Retrieve all actions for the integration.
	 *
	 * @return array[]
	 */
	protected function getActions() {
		return [
			[ self::WEEKLY_MAINTENANCE_CRON_ACTION, [ $this, 'cleanUpMigrationLeftovers' ] ],
		];
	}

	/**
	 * Clean up files left over after migrating from other hosts.
	 */
	public function cleanUpMigrationLeftovers() {
		$leftovers = $this->migrationCleaner->scan();

		if ( empty( $leftovers ) ) {
			return;
		}

		foreach ( $leftovers as $files ) {
			$this->migrationCleaner->remove( $files );
		}
	}
}
