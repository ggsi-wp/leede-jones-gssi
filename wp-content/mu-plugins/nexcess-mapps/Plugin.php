<?php
/**
 * The main Nexcess Managed Apps plugin.
 *
 * This class is responsible for starting up services and loading integrations.
 */

namespace Nexcess\MAPPS;

use Nexcess\MAPPS\Exceptions\IsNotNexcessSiteException;
use Nexcess\MAPPS\Integrations\Integration;
use WP_CLI;

class Plugin {

	/**
	 * All available WP-CLI commands.
	 *
	 * @var mixed[]
	 */
	private $commands = [
		'nxmapps'                   => Commands\Support::class,
		'nxmapps affiliatewp'       => Commands\AffiliateWP::class,
		'nxmapps brainstormforce'   => Commands\BrainstormForce::class,
		'nxmapps cache'             => Commands\Cache::class,
		'nxmapps config'            => Commands\Config::class,
		'nxmapps dokan'             => Commands\Dokan::class,
		'nxmapps ithemes'           => Commands\iThemes::class,
		'nxmapps qubely'            => Commands\Qubely::class,
		'nxmapps setup'             => [ Commands\Setup::class, 'setup' ],
		'nxmapps setup:pre-install' => [ Commands\Setup::class, 'preInstallPlugins' ],
		'nxmapps setup:woocommerce' => [ Commands\Setup::class, 'woocommerce' ],
		'nxmapps storebuilder'      => Commands\StoreBuilder::class,
		'nxmapps vc'                => Commands\VisualComparison::class,
		'nxmapps wp-all-import-pro' => Commands\WPAllImportPro::class,
	];

	/**
	 * The Container instance.
	 *
	 * @var \Nexcess\MAPPS\Container
	 */
	private $container;

	/**
	 * All available integrations.
	 *
	 * @var array[]
	 */
	private $integrations = [
		/*
		 * Integrations here will always be loaded, regardless of whether or not this is running
		 * on a MAPPS site.
		 */
		'global' => [
			Integrations\PHPCompatibility::class,
		],

		/*
		 * These integrations will never be instantiated unless we're running on MAPPS.
		 */
		'mapps'  => [
			Integrations\Admin::class,
			Integrations\Cache::class,
			Integrations\Cron::class,
			Integrations\Dashboard::class,
			Integrations\Debug::class,
			Integrations\DisplayEnvironment::class,
			Integrations\ErrorHandling::class,
			Integrations\Fail2Ban::class,
			Integrations\Feedback::class,
			Integrations\Jetpack::class,
			Integrations\Maintenance::class,
			Integrations\ObjectCache::class,
			Integrations\OPcache::class,
			Integrations\PageCache::class,
			Integrations\PluginInstaller::class,
			Integrations\Recapture::class,
			Integrations\RegressionSites::class,
			Integrations\SiteHealth::class,
			Integrations\StagingSites::class,
			Integrations\StoreBuilder::class,
			Integrations\Support::class,
			Integrations\SupportUsers::class,
			Integrations\Telemetry::class,
			Integrations\Themes::class,
			Integrations\Updates::class,
			Integrations\Varnish::class,
			Integrations\VisualComparison::class,
			Integrations\WooCommerce::class,
			Integrations\WooCommerceUpperLimits::class,
		],
	];

	/**
	 * The Settings instance.
	 *
	 * @var \Nexcess\MAPPS\Settings
	 */
	private $settings;

	/**
	 * Instantiate the class.
	 *
	 * @param \Nexcess\MAPPS\Container $container
	 * @param \Nexcess\MAPPS\Settings  $settings
	 */
	public function __construct( Container $container, Settings $settings ) {
		$this->container = $container;
		$this->settings  = $settings;
	}

	/**
	 * Bootstrap the plugin.
	 *
	 * This method is responsible for orchestrating the setup of the plugin:
	 *
	 * 1. Define any necessary constants.
	 * 2. Load registered integrations.
	 * 3. Load registered WP-CLI commands (if WP-CLI is available).
	 *
	 * @throws \Nexcess\MAPPS\Exceptions\IsNotNexcessSiteException if bootstrapping on a non-MAPPS site.
	 */
	public function bootstrap() {
		// Abort if this is not an Nexcess Managed Apps site.
		if ( ! $this->settings->is_nexcess_site && ( ! defined( 'WP_CLI' ) || ! constant( 'WP_CLI' ) ) ) {
			throw new IsNotNexcessSiteException( 'Does not appear to be an Nexcess Managed Apps site.' );
		}

		$this->defineConstants();
		$this->loadIntegrations();
		$this->loadCommands();
	}

	/**
	 * Define constants for legacy integrations.
	 *
	 * Eventually, these constants should be unnecessary and removed.
	 */
	protected function defineConstants() {
		defined( 'WP_FAIL2BAN_BLOCK_USER_ENUMERATION' ) || define( 'WP_FAIL2BAN_BLOCK_USER_ENUMERATION', true );
		defined( 'ICONIC_DISABLE_DASH' ) || define( 'ICONIC_DISABLE_DASH', true );

		defined( 'NEXCESS_MAPPS_SITE' ) || define( 'NEXCESS_MAPPS_SITE', $this->settings->is_mapps_site );
		defined( 'NEXCESS_MAPPS_PLAN_NAME' ) || define( 'NEXCESS_MAPPS_PLAN_NAME', $this->settings->plan_name );
		defined( 'NEXCESS_MAPPS_PACKAGE_LABEL' ) || define( 'NEXCESS_MAPPS_PACKAGE_LABEL', $this->settings->package_label );
		defined( 'NEXCESS_MAPPS_ENDPOINT' ) || define( 'NEXCESS_MAPPS_ENDPOINT', $this->settings->managed_apps_endpoint );
		defined( 'NEXCESS_MAPPS_TOKEN' ) || define( 'NEXCESS_MAPPS_TOKEN', $this->settings->managed_apps_token );

		if ( $this->settings->is_mwch_site && ! defined( 'NEXCESS_MAPPS_MWCH_SITE' ) ) {
			define( 'NEXCESS_MAPPS_MWCH_SITE', true );
		}

		if ( $this->settings->is_staging_site && ! defined( 'NEXCESS_MAPPS_STAGING_SITE' ) ) {
			define( 'NEXCESS_MAPPS_STAGING_SITE', true );
		}
	}

	/**
	 * Load registered commands.
	 */
	protected function loadCommands() {
		if ( ! defined( 'WP_CLI' ) || ! constant( 'WP_CLI' ) ) {
			return;
		}

		foreach ( $this->commands as $name => $callable ) {
			// Resolve class names through the DI container.
			if ( is_array( $callable ) && is_string( $callable[0] ) ) {
				$callable = [
					$this->container->get( $callable[0] ),
					$callable[1],
				];
			} else {
				$callable = $this->container->get( $callable );
			}

			WP_CLI::add_command( $name, $callable );
		}
	}

	/**
	 * Load each integration.
	 *
	 * This method will loop through $this->integrations and call each integration's
	 * `shouldLoadIntegration()` method; if the result is false, the integration will be unset so
	 * that the garbage collector can clean it up.
	 */
	protected function loadIntegrations() {
		// Always load global integrations.
		array_map( [ $this, 'loadIntegration' ], $this->integrations['global'] );

		// Only load MAPPS integrations if we're on MAPPS.
		if ( $this->settings->is_mapps_site ) {
			array_map( [ $this, 'loadIntegration' ], $this->integrations['mapps'] );
		}
	}

	/**
	 * Load an individual integration.
	 *
	 * This method will resolve an instance of the integration within the DI container, then test
	 * the shouldLoadIntegration() method. If the method returns true, the integration's setup()
	 * method will be called; otherwise, the instance will be destroyed.
	 *
	 * @param string $integration The integration class name.
	 */
	protected function loadIntegration( $integration ) {
		// Create an instance of the integration, then see if it should be loaded.
		$instance = $this->container->get( $integration );

		// If we don't need to load it, discard and return.
		if ( ! $instance->shouldLoadIntegration() ) {
			$this->container->forget( $integration );
			unset( $instance );
			return;
		}

		$instance->setup();
	}
}
