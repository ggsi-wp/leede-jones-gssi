<?php

/**
 * The StoreBuilder welcome screen.
 *
 * @see wp_welcome_panel()
 */

?>

<div class="welcome-panel-content mapps-welcome-panel">
	<h2><?php esc_html_e( 'Congrats on Starting a New Store with StoreBuilder!', 'nexcess-mapps' ); ?></h2>
	<div class="welcome-panel-column-container">
		<div class="welcome-panel-column welcome-panel-intro">
			<div class="mapps-responsive-video">
				<iframe src="https://www.youtube.com/embed/MGqmaGjd5lQ?modestbranding=1&rel=0" frameborder="0" allow="encrypted-media;" allowfullscreen></iframe>
			</div>
		</div>

		<div class="welcome-panel-column">
			<h3><?php esc_html_e( 'Ready to get started?', 'nexcess-mapps' ); ?></h3>
			<ol>
				<?php if ( is_plugin_active( 'wp101/wp101.php' ) ) : ?>
					<li><?php echo wp_kses_post( sprintf(
						/* Translators: %1$s is the WP101 admin page URL. */
						__( 'Learn more about WooCommerce by <a href="%1$s">watching these videos.</a>', 'nexcess-mapps' ),
						esc_url( admin_url( 'admin.php?page=wp101' ) )
					) ); ?></li>
				<?php endif; ?>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is a URL explaining payment gateway configuration. */
					__( '<a href="%1$s">Learn about</a> & configure your <a href="%2$s">Payment Gateway</a>.', 'nexcess-mapps' ),
					'https://nnus-stg.nxswd.net/storebuilder/resources/accepting-major-credit/',
					esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout' ) )
				) ); ?></li>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is the WooCommerce checkout settings page URL. */
					__( 'Customize your <a href="%1$s">Tax and Shipping settings.</a>', 'nexcess-mapps' ),
					esc_url( admin_url( 'admin.php?page=wc-settings&tab=checkout' ) )
				) ); ?></li>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is the "New Product" URL. */
					__( 'Create your <a href="%1$s">first product.</a>', 'nexcess-mapps' ),
					esc_url( admin_url( 'post-new.php?post_type=product' ) )
				) ); ?></li>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is the "New Product" URL. */
					__( 'Optimize your <a href="%1$s">store\'s <abbr title="Search Engine Optimization">SEO</abbr>.</a>', 'nexcess-mapps' ),
					'https://nnus-stg.nxswd.net/storebuilder/resources/optimize-store-seo/'
				) ); ?></li>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is the "New Product" URL. */
					__( 'Start promoting your products <a href="%1$s">on social.</a>', 'nexcess-mapps' ),
					'#'
				) ); ?></li>
				<li><?php echo wp_kses_post( sprintf(
					/* Translators: %1$s is the "New Product" URL. */
					__( 'What to change the <a href="%1$s">look of your pages?</a>', 'nexcess-mapps' ),
					esc_url( admin_url( 'edit.php?post_type=page' ) )
				) ); ?></li>
			</ol>
			<p><a class="button button-primary button-hero" href="<?php echo esc_attr( site_url( '/' ) ); ?>"><?php esc_html_e( 'Visit My Store', 'nexcess-mapps' ); ?></a></p>
		</div>
	</div>
</div>
