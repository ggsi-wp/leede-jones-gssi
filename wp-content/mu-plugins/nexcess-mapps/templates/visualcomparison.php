<?php
/**
 * The Visual Comparison dashboard tab.
 */

use Nexcess\MAPPS\Integrations\VisualComparison;

?>

<div class="mapps-layout-fluid-deferred">
	<div class="mapps-primary">
		<h2><?php esc_html_e( 'Visual Comparison', 'nexcess-mapps' ); ?></h2>
		<p><?php esc_html_e( 'The following paths will be inspected after each plugin update:', 'nexcess-mapps' ); ?></p>

		<form method="POST" action="<?php echo esc_attr( admin_url( 'options.php' ) ); ?>">
			<div id="mapps-visual-comparison-urls">
				<!-- Root element for the VisualComparisonUrls React component. -->
			</div>

			<?php settings_fields( VisualComparison::SETTINGS_GROUP ); ?>
			<input type="hidden" name="_wp_http_referer" value="<?php echo esc_attr( stripslashes( $_SERVER['REQUEST_URI'] ) ); ?>#visual-comparison" />
			<?php submit_button(); ?>
		</form>
	</div>

	<div class="mapps-sidebar card">
		<h3><?php esc_html_e( 'About Visual Comparison', 'nexcess-mapps' ); ?></h3>
		<p><?php esc_html_e( 'Occasionally, updating a plugin can cause major changes to the appearance or behavior of your site.', 'nexcess-mapps' ); ?></p>
		<p><?php esc_html_e( 'We don\'t like that kind of surprise around here, so we perform visual regression testing on key pages of your site.', 'nexcess-mapps' ); ?></p>
		<p><?php esc_html_e( 'Before upgrading anything on a live site, we create a copy of your site, then take screenshots before and after the plugin update; if anything has changed, we hold back the update and let you know.', 'nexcess-mapps' ); ?></p>
		<p><a href="https://help.nexcess.net/74095-wordpress/how-to-use-visual-comparison-tool" class="button"><?php esc_html_e( 'Learn More', 'nexcess-mapps' ); ?></a></p>
	</div>
</div>
