<?php

namespace Nexcess\MAPPS\Concerns;

use const Nexcess\MAPPS\VENDOR_DIR;

trait HasWordPressDependencies {

	/**
	 * Determine if the current version of WordPress is at least $version.
	 *
	 * @param string $version The minimum permitted version of WordPress.
	 *
	 * @return bool
	 */
	public function siteIsAtLeastWordPressVersion( $version ) {
		global $wp_version;

		/*
		 * Stable WordPress releases are in x.y.z format, but can have pre-release versions,
		 * e.g. "5.4-RC4-47505-src".
		 *
		 * We want, for example. 5.4-RC4-47505-src to be considered equal to 5.4, so strip out
		 * the pre-release portion.
		 */
		$current = preg_replace( '/-.+$/', '', $wp_version );

		return version_compare( $version, $current, '<=' );
	}

	/**
	 * Determine whether or not the block editor is enabled for the given post type(s).
	 *
	 * If multiple post types are provided, the function will ensure that the editor is enabled
	 * for *all* of the given types.
	 *
	 * @param string[] ...$post_types The post type(s) to check.
	 *
	 * @return bool Whether or not the Block Editor is enabled for all of the given post types.
	 */
	public function blockEditorIsEnabledFor( ...$post_types ) {
		/*
		 * Ensure the use_block_editor_for_post_type() function, which was introduced in WordPress
		 * version 5.0, exists.
		 *
		 * If not (and assuming it hasn't been polyfilled), assume the store is using the Gutenberg
		 * feature plugin, which may or may not work the way we expect.
		 */
		if ( ! function_exists( 'use_block_editor_for_post_type' ) ) {
			return false;
		}

		foreach ( $post_types as $post_type ) {
			if ( ! use_block_editor_for_post_type( $post_type ) ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Verify that a plugin is installed.
	 *
	 * This is a wrapper around WordPress' get_plugins() function, ensuring the necessary
	 * file is installed before checking.
	 *
	 * @see get_plugins()
	 *
	 * @param string $plugin The directory/file path.
	 *
	 * @return bool
	 */
	public function isPluginInstalled( $plugin ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		// Fetch all the plugins we have installed.
		$all_plugins = get_plugins();

		return array_key_exists( $plugin, $all_plugins );
	}

	/**
	 * Determine whether or not a particular mu-plugin is present.
	 *
	 * Since mu-plugins are loaded in alphabetical order, this is necessary to see if mu-plugins
	 * *after* "nexcess-mapps" will be loaded.
	 *
	 * @param string $text_domain The plugin text-domain to check for.
	 *
	 * @return bool Whether or not the given plugin text-domain is present as an mu-plugin.
	 */
	public function isMuPluginInstalled( $text_domain ) {
		if ( ! function_exists( 'get_mu_plugins' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		foreach ( get_mu_plugins() as $plugin ) {
			if ( ! empty( $plugin['TextDomain'] ) && $text_domain === $plugin['TextDomain'] ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Verify that a plugin is both installed and active.
	 *
	 * This is a wrapper around WordPress' is_plugin_active() function, ensuring the necessary
	 * file is loaded before checking.
	 *
	 * @see is_plugin_active()
	 *
	 * @param string $plugin The directory/file path.
	 *
	 * @return bool
	 */
	public function isPluginActive( $plugin ) {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		return is_plugin_active( $plugin );
	}

	/**
	 * Determine if the current request is related to activating the given plugin.
	 *
	 * If we're forcibly activating a plugin in an integration, we want to make sure that
	 * integration is not being loaded on the activation request, or risk failures due to plugin
	 * classes/functions already being defined.
	 *
	 * @param string $plugin The plugin to check for.
	 *
	 * @return bool
	 */
	public function isPluginBeingActivated( $plugin ) {
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		return isset( $_SERVER['PHP_SELF'], $_GET['action'], $_GET['plugin'] )
			&& '/wp-admin/plugins.php' === $_SERVER['PHP_SELF']
			&& 'activate' === $_GET['action']
			&& $plugin === $_GET['plugin'];
		// phpcs:enable
	}

	/**
	 * Load another plugin's bootstrap file.
	 *
	 * @param string $plugin The plugin file, relative to VENDOR_DIR.
	 */
	public function loadPlugin( $plugin ) {
		$file = VENDOR_DIR . $plugin;

		if ( is_readable( $file ) ) {
			require_once $file;

			/**
			 * Fires after the Nexcess MAPPS plugin bootstraps another plugin.
			 *
			 * @param string $file The full filepath to the bootstrap file.
			 */
			do_action( 'Nexcess\\MAPPS\\load_plugin:' . $plugin, $file ); // phpcs:ignore WordPress.NamingConventions.ValidHookName
		} else {
			// phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_trigger_error
			trigger_error(
				/* Translators: %1$s is the integration filepath. */
				esc_html( sprintf( __( 'Unable to load Nexcess Managed Apps integration from %1$s.', 'nexcess-mapps' ), $file ) ),
				E_USER_WARNING
			);
		}
	}
}
