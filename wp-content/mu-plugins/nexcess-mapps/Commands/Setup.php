<?php

namespace Nexcess\MAPPS\Commands;

use Nexcess\MAPPS\Exceptions\InstallationException;
use Nexcess\MAPPS\Integrations\Telemetry;
use Nexcess\MAPPS\Integrations\WooCommerce;
use Nexcess\MAPPS\Services\Installer;
use Nexcess\MAPPS\Settings;
use WC_Install;

/**
 * Commands for preparing a new Nexcess Managed Apps site.
 */
class Setup extends Command {
	use Concerns\ManagesCacheEnabler;

	/**
	 * @var \Nexcess\MAPPS\Services\Installer
	 */
	private $installer;

	/**
	 * @var \Nexcess\MAPPS\Settings
	 */
	private $settings;

	/**
	 * @var \Nexcess\MAPPS\Integrations\WooCommerce
	 */
	private $woocommerce;

	/**
	 * Create a new instance of the Setup command class.
	 *
	 * @param \Nexcess\MAPPS\Settings                 $settings
	 * @param \Nexcess\MAPPS\Services\Installer       $installer
	 * @param \Nexcess\MAPPS\Integrations\WooCommerce $woocommerce
	 */
	public function __construct( Settings $settings, Installer $installer, WooCommerce $woocommerce ) {
		$this->settings    = $settings;
		$this->installer   = $installer;
		$this->woocommerce = $woocommerce;
	}

	/**
	 * Set up a new Nexcess Managed Apps site.
	 *
	 * This command should automatically detect the type of Nexcess Managed Apps environment and apply the
	 * appropriate setup commands.
	 */
	public function setup() {
		$this->verifyIsMappsSite();

		$this->step( 'Ensuring WordPress core and all existing plugins and themes are up-to-date' );
		$this->wp( 'core update' );
		$this->wp( 'core update-db', [
			'launch' => true,
		] );
		$this->wp( 'plugin update --all --format=summary' );
		$this->wp( 'theme update --all --format=summary' );

		// Pre-install plugins, based on the current site's plan.
		$this->preInstallPlugins();

		// Install and configure Cache Enabler.
		$this->configureCacheEnabler();

		if ( $this->settings->is_mwch_site ) {
			$this->woocommerce();
		}

		// Send initial telemetry data.
		do_action( Telemetry::REPORT_CRON_ACTION );
	}

	/**
	 * Set up a new Managed WooCommerce site.
	 */
	public function woocommerce() {
		$this->verifyIsMappsSite();

		$this->step( 'Configuring WooCommerce...' );

		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			return $this->warning( 'Unable to configure WooCommerce, as WooCommerce is not active on this site.' );
		}

		// Set default options.
		$this->log( '- Setting default options' );
		foreach ( $this->woocommerce->getDefaultOptions() as $key => $value ) {
			update_option( $key, $value );
		}

		if ( $this->settings->is_storebuilder ) {
			$this->line( '- Ingesting content from StoreBuilder' )
				->wp( 'nxmapps storebuilder build' );
		} else {
			$this->wp( 'theme install --activate astra' );
		}

		$this->log( '- Creating default pages' );
		WC_Install::create_pages();

		// Set default user meta.
		$this->log( '- Setting default user meta' );
		add_user_meta( 1, 'is_disable_paypal_marketing_solutions_notice', true );

		// Create default shipping zones.
		$this->log( '- Creating default shipping zones' );
		$this->woocommerce->createDefaultShippingZones();

		$this->newline()->success( 'Default WooCommerce configurations have been applied.' );
	}

	/**
	 * Pre-install and license plugins that should be present based on the site's plan.
	 */
	public function preInstallPlugins() {
		$this->verifyIsMappsSite();

		$this->step( 'Pre-installing plugins for the site\'s plan...' );
		$plugins = $this->installer->getPreinstallPlugins();
		$errors  = false;

		foreach ( $plugins as $plugin ) {
			try {
				$this->log( sprintf( '- Installing %1$s', $plugin->identity ) );
				$this->installer->install( $plugin->id );

				if ( 'none' !== $plugin->license_type ) {
					$this->debug( sprintf(
						'- Licensing %1$s (%2$s)',
						$plugin->identity,
						$plugin->license_type
					) );
					$this->installer->license( $plugin->id );
				}
			} catch ( InstallationException $e ) {
				$this->warning( $e->getMessage() );
				$errors = true;
			}
		}

		if ( $errors ) {
			$this->error( 'One or more plugins could not be pre-installed.', false );
		}
	}

	/**
	 * Verify that this is being run on a MAPPS site.
	 *
	 * If not, a message will be displayed and the script will exit with the given code.
	 *
	 * @param int $code The exit code to use if the check fails.
	 */
	protected function verifyIsMappsSite( $code = 0 ) {
		if ( $this->settings->is_mapps_site ) {
			return;
		}

		$this->warning( 'This does not appear to be a MAPPS site, aborting.' )
			->halt( $code );
	}
}
