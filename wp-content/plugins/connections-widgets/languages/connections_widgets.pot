#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Connections Widgets\n"
"POT-Creation-Date: 2018-04-24 11:28-0400\n"
"PO-Revision-Date: 2013-07-23 20:29-0500\n"
"Last-Translator: Steven A. Zahm <helpdesk@connections-pro.com>\n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.7\n"
"X-Poedit-KeywordsList: __;_e;_n;_x;esc_html_e;esc_html__;esc_attr_e;"
"esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SearchPath-0: .\n"

#: connections_widgets.php:248
msgid "Connections: Widget Shortcodes"
msgstr ""

#: connections_widgets.php:250
msgid ""
"This widget area can be used for the &#91;cn_widget&#93; shortcode."
msgstr ""

#: connections_widgets.php:312
msgid "Shortcode"
msgstr ""

#: connections_widgets.php:312
msgid "Please click \"Save\" to genreate the shortcode."
msgstr ""

#: connections_widgets.php:387
#, php-format
msgid "Widget %s ID found but it is inactive."
msgstr ""

#: includes/class.widget-category.php:31
msgid "A list of categories."
msgstr ""

#: includes/class.widget-category.php:36
msgid "Categories"
msgstr ""

#: includes/class.widget-category.php:154
#: includes/class.widget-category.php:440
msgid "Directory Categories"
msgstr ""

#: includes/class.widget-category.php:162
#: includes/class.widget-list.php:151
#: includes/class.widget-recently-added.php:141
#: includes/class.widget-recently-modified.php:135
#: includes/class.widget-search.php:62
#: includes/class.widget-todays-anniversaries.php:137
#: includes/class.widget-todays-birthdays.php:137
#: includes/class.widget-upcoming-anniversaries.php:137
#: includes/class.widget-upcoming-birthdays.php:137
msgid "Title:"
msgstr ""

#: includes/class.widget-category.php:169
msgid "Display:"
msgstr ""

#: includes/class.widget-category.php:181
msgid "All."
msgstr ""

#: includes/class.widget-category.php:185
msgid "Include or exclude specific categories."
msgstr ""

#: includes/class.widget-category.php:189
msgid "Display specific category parent."
msgstr ""

#: includes/class.widget-category.php:223
msgid "Select categories:"
msgstr ""

#: includes/class.widget-category.php:224
#: includes/class.widget-list.php:178
#: includes/class.widget-list.php:222
#: includes/class.widget-recently-added.php:168
#: includes/class.widget-recently-added.php:212
#: includes/class.widget-recently-modified.php:162
#: includes/class.widget-recently-modified.php:206
#: includes/class.widget-todays-anniversaries.php:164
#: includes/class.widget-todays-anniversaries.php:208
#: includes/class.widget-todays-birthdays.php:164
#: includes/class.widget-todays-birthdays.php:208
#: includes/class.widget-upcoming-anniversaries.php:164
#: includes/class.widget-upcoming-anniversaries.php:208
#: includes/class.widget-upcoming-birthdays.php:164
#: includes/class.widget-upcoming-birthdays.php:208
msgid "Click to Select Categories"
msgstr ""

#: includes/class.widget-category.php:243
msgid "Include only the categories selected above."
msgstr ""

#: includes/class.widget-category.php:247
msgid "Exclude the categories selected above."
msgstr ""

#: includes/class.widget-category.php:269
msgid "Limit to the selected category:"
msgstr ""

#: includes/class.widget-category.php:270
msgid "Click to Select Category"
msgstr ""

#: includes/class.widget-category.php:291
msgid "Display only the immediate children of the selected category."
msgstr ""

#: includes/class.widget-category.php:295
msgid "Display all descendants of the selected category."
msgstr ""

#: includes/class.widget-category.php:318
msgid "Show Count?"
msgstr ""

#: includes/class.widget-category.php:332
msgid "Hide empty?"
msgstr ""

#: includes/class.widget-category.php:352
msgid ""
"The number of levels deep of the category tree that should be shown."
msgstr ""

#: includes/class.widget-category.php:360
msgid ""
"<strong>NOTE:</strong> The default is <code>0</code>. When set to "
"<code>0</code>, the entire category tree will be shown."
msgstr ""

#: includes/class.widget-category.php:373
msgid "Default"
msgstr ""

#: includes/class.widget-category.php:374
msgid "Accordion"
msgstr ""

#: includes/class.widget-category.php:375
msgid "Expand"
msgstr ""

#: includes/class.widget-category.php:377
msgid "Display Style:"
msgstr ""

#: includes/class.widget-list.php:27
msgid "A list of entries."
msgstr ""

#: includes/class.widget-list.php:32
msgid "List"
msgstr ""

#: includes/class.widget-list.php:95
#: includes/class.widget-list.php:249
#: includes/class.widget-list.php:510
#: includes/class.widget-recently-added.php:233
#: includes/class.widget-recently-modified.php:227
#: includes/class.widget-todays-anniversaries.php:229
#: includes/class.widget-todays-birthdays.php:229
#: includes/class.widget-upcoming-anniversaries.php:229
#: includes/class.widget-upcoming-birthdays.php:229
msgid "None"
msgstr ""

#: includes/class.widget-list.php:96
#: includes/class.widget-list.php:250
#: includes/class.widget-list.php:511
#: includes/class.widget-recently-added.php:235
#: includes/class.widget-recently-modified.php:229
#: includes/class.widget-todays-anniversaries.php:230
#: includes/class.widget-todays-birthdays.php:230
#: includes/class.widget-upcoming-anniversaries.php:231
#: includes/class.widget-upcoming-birthdays.php:231
msgid "Logo"
msgstr ""

#: includes/class.widget-list.php:97
#: includes/class.widget-list.php:251
#: includes/class.widget-list.php:512
#: includes/class.widget-recently-added.php:236
#: includes/class.widget-recently-modified.php:230
#: includes/class.widget-todays-anniversaries.php:231
#: includes/class.widget-todays-birthdays.php:231
#: includes/class.widget-upcoming-anniversaries.php:232
#: includes/class.widget-upcoming-birthdays.php:232
msgid "Photo"
msgstr ""

#: includes/class.widget-list.php:143
#: includes/class.widget-list.php:479
msgid "Directory Entries"
msgstr ""

#: includes/class.widget-list.php:177
#: includes/class.widget-recently-added.php:167
#: includes/class.widget-recently-modified.php:161
#: includes/class.widget-todays-anniversaries.php:163
#: includes/class.widget-todays-birthdays.php:163
#: includes/class.widget-upcoming-anniversaries.php:163
#: includes/class.widget-upcoming-birthdays.php:163
msgid "Limit to the selected categories:"
msgstr ""

#: includes/class.widget-list.php:194
#: includes/class.widget-recently-added.php:184
#: includes/class.widget-recently-modified.php:178
#: includes/class.widget-todays-anniversaries.php:180
#: includes/class.widget-todays-birthdays.php:180
#: includes/class.widget-upcoming-anniversaries.php:180
#: includes/class.widget-upcoming-birthdays.php:180
msgid "Must be in all categories selected above?"
msgstr ""

#: includes/class.widget-list.php:221
#: includes/class.widget-recently-added.php:211
#: includes/class.widget-recently-modified.php:205
#: includes/class.widget-todays-anniversaries.php:207
#: includes/class.widget-todays-birthdays.php:207
#: includes/class.widget-upcoming-anniversaries.php:207
#: includes/class.widget-upcoming-birthdays.php:207
msgid "Exclude the selected categories:"
msgstr ""

#: includes/class.widget-list.php:238
msgid "Randomize list?"
msgstr ""

#: includes/class.widget-list.php:261
#: includes/class.widget-recently-added.php:228
#: includes/class.widget-recently-modified.php:222
#: includes/class.widget-todays-anniversaries.php:224
#: includes/class.widget-todays-birthdays.php:224
msgid "Show Image:"
msgstr ""

#: includes/class.widget-list.php:279
#: includes/class.widget-recently-added.php:251
#: includes/class.widget-recently-modified.php:245
#: includes/class.widget-todays-anniversaries.php:246
#: includes/class.widget-todays-birthdays.php:246
#: includes/class.widget-upcoming-anniversaries.php:247
#: includes/class.widget-upcoming-birthdays.php:247
msgid "Image Size:"
msgstr ""

#: includes/class.widget-list.php:295
#: includes/class.widget-recently-added.php:267
#: includes/class.widget-recently-modified.php:261
#: includes/class.widget-todays-anniversaries.php:262
#: includes/class.widget-todays-birthdays.php:262
#: includes/class.widget-upcoming-anniversaries.php:263
#: includes/class.widget-upcoming-birthdays.php:263
msgid "Image Style:"
msgstr ""

#: includes/class.widget-list.php:300
#: includes/class.widget-recently-added.php:272
#: includes/class.widget-recently-modified.php:266
#: includes/class.widget-todays-anniversaries.php:267
#: includes/class.widget-todays-birthdays.php:267
#: includes/class.widget-upcoming-anniversaries.php:268
#: includes/class.widget-upcoming-birthdays.php:268
msgid "Circle"
msgstr ""

#: includes/class.widget-list.php:301
#: includes/class.widget-recently-added.php:273
#: includes/class.widget-recently-modified.php:267
#: includes/class.widget-todays-anniversaries.php:268
#: includes/class.widget-todays-birthdays.php:268
#: includes/class.widget-upcoming-anniversaries.php:269
#: includes/class.widget-upcoming-birthdays.php:269
msgid "Square"
msgstr ""

#: includes/class.widget-list.php:313
#: includes/class.widget-recently-added.php:285
#: includes/class.widget-recently-modified.php:279
#: includes/class.widget-todays-anniversaries.php:280
#: includes/class.widget-todays-birthdays.php:280
#: includes/class.widget-upcoming-anniversaries.php:281
#: includes/class.widget-upcoming-birthdays.php:281
msgid "Image Crop Mode:"
msgstr ""

#: includes/class.widget-list.php:318
#: includes/class.widget-recently-added.php:290
#: includes/class.widget-recently-modified.php:284
#: includes/class.widget-todays-anniversaries.php:285
#: includes/class.widget-todays-birthdays.php:285
#: includes/class.widget-upcoming-anniversaries.php:286
#: includes/class.widget-upcoming-birthdays.php:286
msgid "Resize to fit, no crop."
msgstr ""

#: includes/class.widget-list.php:319
#: includes/class.widget-recently-added.php:291
#: includes/class.widget-recently-modified.php:285
#: includes/class.widget-todays-anniversaries.php:286
#: includes/class.widget-todays-birthdays.php:286
#: includes/class.widget-upcoming-anniversaries.php:287
#: includes/class.widget-upcoming-birthdays.php:287
msgid "Best fit, resize and crop."
msgstr ""

#: includes/class.widget-list.php:320
#: includes/class.widget-recently-added.php:292
#: includes/class.widget-recently-modified.php:286
#: includes/class.widget-todays-anniversaries.php:287
#: includes/class.widget-todays-birthdays.php:287
#: includes/class.widget-upcoming-anniversaries.php:288
#: includes/class.widget-upcoming-birthdays.php:288
msgid "Resize proportionally to fit, no crop, add margin."
msgstr ""

#: includes/class.widget-list.php:321
#: includes/class.widget-recently-added.php:293
#: includes/class.widget-recently-modified.php:287
#: includes/class.widget-todays-anniversaries.php:288
#: includes/class.widget-todays-birthdays.php:288
#: includes/class.widget-upcoming-anniversaries.php:289
#: includes/class.widget-upcoming-birthdays.php:289
msgid "Resize proportionally to fit, no crop, no margin."
msgstr ""

#: includes/class.widget-list.php:329
msgid ""
"<strong>NOTE:</strong> Selecting the <code>Circle</code> image "
"style and choosing the crop mode <code>Resize proportionally to "
"fit, no crop, no margin.</code> will likely result in non-circle "
"presentation of the image thumbnail."
msgstr ""

#: includes/class.widget-list.php:338
#: includes/class.widget-recently-added.php:310
#: includes/class.widget-recently-modified.php:304
#: includes/class.widget-todays-anniversaries.php:305
#: includes/class.widget-todays-birthdays.php:305
#: includes/class.widget-upcoming-anniversaries.php:306
#: includes/class.widget-upcoming-birthdays.php:306
msgid "Show placeholder if no image is available?"
msgstr ""

#: includes/class.widget-list.php:351
#: includes/class.widget-list.php:529
#: includes/class.widget-recently-added.php:323
#: includes/class.widget-recently-added.php:490
#: includes/class.widget-recently-modified.php:317
#: includes/class.widget-recently-modified.php:484
#: includes/class.widget-todays-anniversaries.php:318
#: includes/class.widget-todays-anniversaries.php:531
#: includes/class.widget-todays-birthdays.php:318
#: includes/class.widget-todays-birthdays.php:530
#: includes/class.widget-upcoming-anniversaries.php:319
#: includes/class.widget-upcoming-anniversaries.php:531
#: includes/class.widget-upcoming-birthdays.php:319
#: includes/class.widget-upcoming-birthdays.php:531
msgid "No Logo"
msgstr ""

#: includes/class.widget-list.php:355
#: includes/class.widget-list.php:533
#: includes/class.widget-recently-added.php:327
#: includes/class.widget-recently-added.php:494
#: includes/class.widget-recently-modified.php:321
#: includes/class.widget-recently-modified.php:488
#: includes/class.widget-todays-anniversaries.php:322
#: includes/class.widget-todays-anniversaries.php:535
#: includes/class.widget-todays-birthdays.php:322
#: includes/class.widget-todays-birthdays.php:534
#: includes/class.widget-upcoming-anniversaries.php:323
#: includes/class.widget-upcoming-anniversaries.php:535
#: includes/class.widget-upcoming-birthdays.php:323
#: includes/class.widget-upcoming-birthdays.php:535
msgid "No Photo"
msgstr ""

#: includes/class.widget-list.php:366
#: includes/class.widget-recently-added.php:338
#: includes/class.widget-recently-modified.php:332
#: includes/class.widget-todays-anniversaries.php:333
#: includes/class.widget-todays-birthdays.php:333
#: includes/class.widget-upcoming-anniversaries.php:334
#: includes/class.widget-upcoming-birthdays.php:334
msgid "Image Placeholder Text:"
msgstr ""

#: includes/class.widget-list.php:386
#: includes/class.widget-recently-added.php:358
#: includes/class.widget-recently-modified.php:352
#: includes/class.widget-todays-anniversaries.php:353
#: includes/class.widget-todays-birthdays.php:353
#: includes/class.widget-upcoming-anniversaries.php:354
#: includes/class.widget-upcoming-birthdays.php:354
msgid "Name format:"
msgstr ""

#: includes/class.widget-list.php:397
#: includes/class.widget-recently-added.php:369
#: includes/class.widget-recently-modified.php:363
#: includes/class.widget-todays-anniversaries.php:364
#: includes/class.widget-todays-birthdays.php:364
#: includes/class.widget-upcoming-anniversaries.php:365
#: includes/class.widget-upcoming-birthdays.php:365
msgid ""
"<strong>NOTE:</strong> The name format will not affect the entry "
"types of Organization and Family."
msgstr ""

#: includes/class.widget-list.php:405
#: includes/class.widget-recently-added.php:377
#: includes/class.widget-recently-modified.php:371
#: includes/class.widget-todays-anniversaries.php:372
#: includes/class.widget-todays-birthdays.php:372
#: includes/class.widget-upcoming-anniversaries.php:373
#: includes/class.widget-upcoming-birthdays.php:373
msgid "Link to entry?"
msgstr ""

#: includes/class.widget-list.php:422
msgid "The maximum number display:"
msgstr ""

#: includes/class.widget-recently-added.php:28
msgid "A list of recently added entries."
msgstr ""

#: includes/class.widget-recently-added.php:33
#: includes/class.widget-recently-added.php:133
#: includes/class.widget-recently-added.php:451
msgid "Recently Added"
msgstr ""

#: includes/class.widget-recently-added.php:234
#: includes/class.widget-recently-modified.php:228
#: includes/class.widget-upcoming-anniversaries.php:230
#: includes/class.widget-upcoming-birthdays.php:230
msgid "Date"
msgstr ""

#: includes/class.widget-recently-added.php:301
#: includes/class.widget-recently-modified.php:295
#: includes/class.widget-todays-anniversaries.php:296
#: includes/class.widget-todays-birthdays.php:296
#: includes/class.widget-upcoming-anniversaries.php:297
#: includes/class.widget-upcoming-birthdays.php:297
msgid ""
"<strong>NOTE:</strong> Selecting the <code>Circle</code> image "
"style and choosing the crop mode <code>Resize proportionally to "
"fit, no crop, no margin.</code> will likely result in non-circle "
"presentatoin of the image thumbnail."
msgstr ""

#: includes/class.widget-recently-added.php:394
msgid "The maximum number of recently added to display:"
msgstr ""

#: includes/class.widget-recently-modified.php:23
msgid "A list of recently modified entries."
msgstr ""

#: includes/class.widget-recently-modified.php:28
#: includes/class.widget-recently-modified.php:127
msgid "Recently Modified"
msgstr ""

#: includes/class.widget-recently-modified.php:388
msgid "The maximum number of recently modified to display:"
msgstr ""

#: includes/class.widget-recently-modified.php:445
msgid "Modified Modified"
msgstr ""

#: includes/class.widget-search.php:14
#: includes/class.widget-search.php:19
msgid "Simple Search"
msgstr ""

#: includes/class.widget-search.php:58
#: includes/class.widget-search.php:96
msgid "Search Directory"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:23
msgid "A list of entries with anniversaries today."
msgstr ""

#: includes/class.widget-todays-anniversaries.php:28
#: includes/class.widget-todays-anniversaries.php:129
#: includes/class.widget-todays-anniversaries.php:492
msgid "Anniversaries Today"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:397
msgid "Display widget when there are no anniversaries today?"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:414
#: includes/class.widget-todays-birthdays.php:414
#: includes/class.widget-upcoming-anniversaries.php:415
#: includes/class.widget-upcoming-birthdays.php:415
msgid "No result message:"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:418
#: includes/class.widget-todays-anniversaries.php:547
#: includes/class.widget-upcoming-anniversaries.php:547
msgid "No Anniversaries Today"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:432
#: includes/class.widget-todays-birthdays.php:432
msgid "The maximum number to display:"
msgstr ""

#: includes/class.widget-todays-anniversaries.php:440
#: includes/class.widget-todays-birthdays.php:440
msgid "<strong>NOTE:</strong> Set to <code>0</code> for unlimited."
msgstr ""

#: includes/class.widget-todays-birthdays.php:23
msgid "A list of entries with birthdays today."
msgstr ""

#: includes/class.widget-todays-birthdays.php:28
#: includes/class.widget-todays-birthdays.php:129
#: includes/class.widget-todays-birthdays.php:491
msgid "Birthdays Today"
msgstr ""

#: includes/class.widget-todays-birthdays.php:397
msgid "Display widget when there are no birthdays today?"
msgstr ""

#: includes/class.widget-todays-birthdays.php:418
#: includes/class.widget-todays-birthdays.php:546
#: includes/class.widget-upcoming-birthdays.php:547
msgid "No Birthdays Today"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:23
msgid "A list of entries with upcoming anniversaries."
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:28
#: includes/class.widget-upcoming-anniversaries.php:129
#: includes/class.widget-upcoming-anniversaries.php:492
msgid "Upcoming Anniversaries"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:224
#: includes/class.widget-upcoming-birthdays.php:224
msgid "Show Date or Image:"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:398
msgid "Display widget when there are no upcoming anniversaries?"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:419
msgid "No Upcoming Anniversaries"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:433
msgid ""
"The maximum number of days to look ahead for upcoming anniversaries:"
msgstr ""

#: includes/class.widget-upcoming-anniversaries.php:441
#: includes/class.widget-upcoming-birthdays.php:441
msgid "<strong>NOTE:</strong> The default is <code>30</code> days."
msgstr ""

#: includes/class.widget-upcoming-birthdays.php:23
msgid "A list of entries with upcoming birthdays."
msgstr ""

#: includes/class.widget-upcoming-birthdays.php:28
#: includes/class.widget-upcoming-birthdays.php:129
#: includes/class.widget-upcoming-birthdays.php:492
msgid "Upcoming Birthdays"
msgstr ""

#: includes/class.widget-upcoming-birthdays.php:398
msgid "Display widget when there are no upcoming birthdays?"
msgstr ""

#: includes/class.widget-upcoming-birthdays.php:419
msgid "No Upcoming Birthdays"
msgstr ""

#: includes/class.widget-upcoming-birthdays.php:433
msgid ""
"The maximum number of days to look ahead for upcoming birthdays:"
msgstr ""

#: templates/recently-modified/recently-modified.php:34
msgid "Widget: Recently Modified"
msgstr ""

#: templates/recently-modified/recently-modified.php:40
msgid "Widget that displays the recently modified entries."
msgstr ""

#: templates/todays-anniversaries/todays_anniversaries.php:34
msgid "Widget: Today's Anniversaries"
msgstr ""

#: templates/todays-anniversaries/todays_anniversaries.php:40
msgid "Widget that displays the today's anniversaries."
msgstr ""

#: templates/todays-birthdays/todays_birthdays.php:34
msgid "Widget: Today's Birthdays"
msgstr ""

#: templates/todays-birthdays/todays_birthdays.php:40
msgid "Widget that displays the today's birthdays."
msgstr ""

#: templates/upcoming-anniversaries/upcoming_anniversaries.php:34
msgid "Widget: Upcoming Anniversaries"
msgstr ""

#: templates/upcoming-anniversaries/upcoming_anniversaries.php:40
msgid "Widget that displays the upcoming anniversaries."
msgstr ""

#: templates/upcoming-birthdays/upcoming_birthdays.php:34
msgid "Widget: Upcoming Birthdays"
msgstr ""

#: templates/upcoming-birthdays/upcoming_birthdays.php:40
msgid "Widget that displays the upcoming birthdays."
msgstr ""
