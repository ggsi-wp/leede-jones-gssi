<?php

/**
 * @package    Connections Widget Pack
 * @subpackage Template : Recently Modified
 * @author     Steven A. Zahm
 * @since      2.0
 * @license    GPL-2.0+
 * @link       http://connections-pro.com
 * @copyright  2013 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Widget Pack - Template
 * Plugin URI:        http://connections-pro.com
 * Description:       The Recently Modified Widget Template.
 * Version:           2.0
 * Author:            Steven A. Zahm
 * Author URI:        http://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'CN_Widget_Pack_Recently_Modified_Template' ) ) {

	class CN_Widget_Pack_Recently_Modified_Template {

		public static function register() {

			$atts = array(
				'class'       => __CLASS__,
				'name'        => __( 'Widget: Recently Modified', 'connections_widgets' ),
				'slug'        => 'widget-recently-modified',
				'type'        => 'widget',
				'version'     => '2.0',
				'author'      => 'Steven A. Zahm',
				'authorURL'   => 'connections-pro.com',
				'description' => __( 'Widget that displays the recently modified entries.', 'connections_widgets' ),
				'custom'      => FALSE,
				'path'        => plugin_dir_path( __FILE__ ),
				'url'         => plugin_dir_url( __FILE__ ),
				'thumbnail'   => '',
				'parts'       => array( 'css' => 'styles.css' ),
				);

			cnTemplateFactory::register( $atts );
		}

		/**
		 * @param cnTemplate $template
		 */
		public function __construct( $template ) {

			$this->template = $template;

			$template->part( array( 'tag' => 'card', 'type' => 'action', 'callback' => array( __CLASS__, 'card' ) ) );
		}

		/**
		 * @param cnOutput   $entry
		 * @param cnTemplate $template
		 * @param array      $atts
		 */
		public static function card( $entry, $template, $atts ) {

			$style = $atts['img_style'] == 'circle' ? array( '-moz-border-radius' => '50%', '-webkit-border-radius' => '50%', 'border-radius' => '50%' ) : array();

			switch ( $atts['show'] ) {

				case 'date':

					echo '<div class="date" style="width: ' . $atts['img_size'] . 'px; height: ' . $atts['img_size'] . 'px; font-size: ' . ceil( $atts['img_size'] / 2.75 ) . 'px;">';
						echo '<abbr class="month">' . $entry->getFormattedTimeStamp('M') . '</abbr><span class="day">' . $entry->getFormattedTimeStamp('d') . '</span>';
					echo '</div>';
					break;

				case 'logo':
					$entry->getImage(
						array(
							'image'    => 'logo',
							'width'    => $atts['img_size'],
							'height'   => $atts['img_size'],
							'zc'       => $atts['img_crop_mode'],
							'fallback' => array(
								'type'   => $atts['img_placeholder'] ? 'block' : 'none',
								'string' => $atts['str_img_placeholder'],
								),
							'style'    => $style,
							)
						);
					break;

				case 'photo':
					$entry->getImage(
						array(
							'width'    => $atts['img_size'],
							'height'   => $atts['img_size'],
							'zc'       => $atts['img_crop_mode'],
							'fallback' => array(
								'type'   => $atts['img_placeholder'] ? 'block' : 'none',
								'string' => $atts['str_img_placeholder'],
								),
							'style'    => $style,
							)
						);
					break;
			}

			echo '<span class="cn-widget cn-name">' , $entry->getNameBlock( array( 'link' => $atts['link'], 'format' => $atts['name_format'] ) ) , '</span>';

		}

	}

	add_action( 'cn_register_template', array( 'CN_Widget_Pack_Recently_Modified_Template', 'register' ) );
}
