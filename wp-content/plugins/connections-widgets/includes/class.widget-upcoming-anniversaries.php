<?php

/**
 * The upcoming anniversaries widget.
 *
 * @package     Connections Widget Pack
 * @subpackage  Upcoming Anniversaries Widget
 * @copyright   Copyright (c) 2014, Steven A. Zahm
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       2.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class cnWidgetUpcomingAnniversaries extends WP_Widget {

	/**
	 * Register widget.
	 */
	public function __construct() {
		$options = array(
			'description' => __( 'A list of entries with upcoming anniversaries.', 'connections_widgets' )
		);

		parent::__construct(
			'cnw_upcoming_anniversaries',
			'Connections : ' . __( 'Upcoming Anniversaries', 'connections_widgets' ),
			$options
		);
	}

	/**
	 * Registers the widget with the WordPress Widget API.
	 *
	 * @access public
	 * @since  2.0
	 *
	 * @return void
	 */
	public static function register() {

		register_widget( __CLASS__ );
	}

	/**
	 * Process updates from the widget form.
	 *
	 * @access private
	 * @since  1.0
	 * @uses   sanitize_text_field()
	 * @uses   absint()
	 * @uses   cnFragment::clear()
	 * @param array $new
	 * @param array $old
	 *
	 * @return array
	 */
	public function update( $new, $old ) {

		$new['title'] = sanitize_text_field( $new['title'] );

		if ( isset( $new['category'] ) && ! empty( $new['category'] ) ) {

			array_walk( $new['category'], 'absint' );

			$new['category'] = json_encode( $new['category'] );
		}

		if ( isset( $new['exclude_category'] ) && ! empty( $new['exclude_category'] ) ) {

			array_walk( $new['exclude_category'], 'absint' );

			$new['exclude_category'] = json_encode( $new['exclude_category'] );
		}

		$new['name_format'] = sanitize_text_field( $new['name_format'] );

		$new['link'] = isset( $new['link'] ) ? '1' : '0';

		$new['limit'] = filter_var( $new['limit'], FILTER_VALIDATE_INT, array( 'options' => array( 'default' => 30, 'min_range' => 1 ) ) );

		$new['show'] = isset( $new['show'] ) && in_array( $new['show'], array( 'none', 'date', 'logo', 'photo' ) ) ? $new['show'] : 'date';

		$new['img_size']  = isset( $new['img_size'] ) ? absint( $new['img_size'] ) : 48;

		$new['img_style'] = isset( $new['img_style'] ) && in_array( $new['img_style'], array( 'circle', 'square' ) ) ? $new['img_style'] : 'square';

		$new['img_crop_mode'] = isset( $new['img_crop_mode'] ) && in_array( (int) $new['img_crop_mode'], array( 0, 1, 2, 3 ) ) ? $new['img_crop_mode'] : 1;

		$new['img_placeholder'] = isset( $new['img_placeholder'] ) ? '1' : '0';

		if ( $new['img_placeholder'] ) {

			if ( isset( $new['str_img_placeholder'] ) && strlen( $new['str_img_placeholder'] ) > 0 ) $new['str_img_placeholder'] = sanitize_text_field( $new['str_img_placeholder'] );
		}

		$new['display_no_results'] = isset( $new['display_no_results'] ) ? '1' : '0';

		$new['no_results_message'] = sanitize_text_field( $new['no_results_message'] );

		// Clear the widget group fragment cache.
		cnFragment::clear( TRUE, 'cnw_upcoming_anniversaries-' . $this->number );

		return $new;
	}

	/**
	 * Callback to display the widget's settings in the admin.
	 *
	 * @access private
	 * @since  1.0
	 * @uses   esc_attr()
	 * @uses   cnHTML::text()
	 * @uses   get_field_id()
	 * @uses   get_field_name()
	 * @uses   set_query_var()
	 * @uses   cnTemplatePart::category()
	 * @uses   cnHTML::field()
	 * @uses   cnHTML::input()
	 * @uses   wp_enqueue_style()
	 * @uses   wp_enqueue_script()
	 * @uses   add_action()
	 * @param array $instance
	 * @return void
	 */
	public function form( $instance ) {

		$title = isset( $instance['title'] ) && strlen( $instance['title'] ) > 0 ? esc_attr( $instance['title'] ) : __( 'Upcoming Anniversaries', 'connections_widgets' );

		cnHTML::text(
			array(
				'prefix' => '',
				'class'  => 'widefat',
				'id'     => $this->get_field_id('title'),
				'name'   => $this->get_field_name('title'),
				'label'  => __( 'Title:', 'connections_widgets' ),
				'before' => '<p>',
				'after'  => '</p>',
				),
			$title
		);

		// Set the query var `cn-cat` so cnTemplatePart::category() will set the slected options.
		if ( isset( $instance['category'] ) && ! empty( $instance['category'] ) ) {

			$categories = json_decode( $instance['category'], TRUE );

			array_walk( $categories, 'absint' );

			set_query_var( 'cn-cat', $categories );

		} else {

			set_query_var( 'cn-cat', FALSE );
		}

		cnTemplatePart::category(
			array(
				'type'            => 'multiselect',
				'id'              => $this->get_field_id('category'),
				'name'            => $this->get_field_name('category'),
				'label'           => __( 'Limit to the selected categories:', 'connections_widgets' ),
				'default'         => __( 'Click to Select Categories', 'connections_widgets' ),
				'on_change'       => '',
				'enhanced'        => TRUE,
				'style'           => array( 'max-width' => '100%' ),
				'show_select_all' => FALSE,
				'before'          => '<p>',
				'after'           => '</p>',
			)
		);

		cnHTML::field(
			array(
				'type'     => 'checkbox',
				'prefix'   => '',
				'id'       => $this->get_field_id('category_in'),
				'name'     => $this->get_field_name('category_in'),
				'label'    => __( 'Must be in all categories selected above?', 'connections_widgets' ),
				'before'   => '<p>',
				'after'    => '</p>',
				'layout'   => '%field% %label%',
				),
			isset( $instance['category_in'] ) && $instance['category_in'] !== '0' ? '1' : FALSE
		);

		// Set the query var `cn-cat` so cnTemplatePart::category() will set the selected options.
		if ( isset( $instance['exclude_category'] ) && ! empty( $instance['exclude_category'] ) ) {

			$categories = json_decode( $instance['exclude_category'], TRUE );

			array_walk( $categories, 'absint' );

			set_query_var( 'cn-cat', $categories );

		} else {

			set_query_var( 'cn-cat', FALSE );
		}

		cnTemplatePart::category(
			array(
				'type'            => 'multiselect',
				'id'              => $this->get_field_id('exclude_category'),
				'name'            => $this->get_field_name('exclude_category'),
				'label'           => __( 'Exclude the selected categories:', 'connections_widgets' ),
				'default'         => __( 'Click to Select Categories', 'connections_widgets' ),
				'on_change'       => '',
				'enhanced'        => TRUE,
				'style'           => array( 'max-width' => '100%' ),
				'show_select_all' => FALSE,
				'before'          => '<p>',
				'after'           => '</p>',
			)
		);

		cnHTML::field(
			array(
				'type'     => 'select',
				'prefix'   => '',
				'id'       => $this->get_field_id('show'),
				'name'     => $this->get_field_name('show'),
				'label'    => __( 'Show Date or Image:', 'connections_widgets' ),
				'before'   => '<p>',
				'after'    => '</p>',
				'layout'   => '%label% %field%',
				'options'  => array(
					'none'  => __( 'None', 'connections_widgets' ),
					'date'  => __( 'Date', 'connections_widgets' ),
					'logo'  => __( 'Logo', 'connections_widgets' ),
					'photo' => __( 'Photo', 'connections_widgets' ),
					),
				),
			isset( $instance['show'] ) ? $instance['show'] : 'date'
		);

		if ( isset( $instance['show'] ) && in_array( $instance['show'], array( 'logo', 'photo' ) ) ) {

			cnHTML::input(
				array(
					'type'   => 'number',
					'prefix' => '',
					'class'  => 'small-text',
					'id'     => $this->get_field_id('img_size'),
					'name'   => $this->get_field_name('img_size'),
					'label'  => __( 'Image Size:', 'connections_widgets' ),
					'before' => '<p>',
					'after'  => 'px</p>',
					'layout'   => '%label% %field%',
					),
				isset( $instance['img_size'] ) ? absint( $instance['img_size'] ) : 48
			);

			if ( in_array( $instance['show'], array( 'logo', 'photo' ) ) ) {

				cnHTML::field(
					array(
						'type'     => 'select',
						'prefix'   => '',
						'id'       => $this->get_field_id('img_style'),
						'name'     => $this->get_field_name('img_style'),
						'label'    => __( 'Image Style:', 'connections_widgets' ),
						'before'   => '<p>',
						'after'    => '</p>',
						'layout'   => '%label% %field%',
						'options'  => array(
							'circle'  => __( 'Circle', 'connections_widgets' ),
							'square'  => __( 'Square', 'connections_widgets' ),
							),
						),
					isset( $instance['img_style'] ) && in_array( $instance['img_style'], array( 'circle', 'square' ) ) ? $instance['img_style'] : 'square'
				);

				cnHTML::field(
					array(
						'type'     => 'select',
						'prefix'   => '',
						'id'       => $this->get_field_id('img_crop_mode'),
						'name'     => $this->get_field_name('img_crop_mode'),
						'label'    => __( 'Image Crop Mode:', 'connections_widgets' ),
						'before'   => '<p>',
						'after'    => '</p>',
						'layout'   => '%label%<br>%field%',
						'options'  => array(
							'0'  => __( 'Resize to fit, no crop.', 'connections_widgets' ),
							'1'  => __( 'Best fit, resize and crop.', 'connections_widgets' ),
							'2'  => __( 'Resize proportionally to fit, no crop, add margin.', 'connections_widgets' ),
							'3'  => __( 'Resize proportionally to fit, no crop, no margin.', 'connections_widgets' ),
							),
						),
					isset( $instance['img_crop_mode'] ) && in_array( $instance['img_crop_mode'], array( 0, 1, 2, 3 ) ) ? (int) $instance['img_crop_mode'] : 1
				);

				if ( ( isset( $instance['img_style'] ) && $instance['img_style'] == 'circle' ) && ( isset( $instance['img_crop_mode'] ) && $instance['img_crop_mode'] == 3 ) ) {

					echo '<p class="description">' . __( '<strong>NOTE:</strong> Selecting the <code>Circle</code> image style and choosing the crop mode <code>Resize proportionally to fit, no crop, no margin.</code> will likely result in non-circle presentatoin of the image thumbnail.', 'connections_widgets' ) . '</p>';
				}

				cnHTML::field(
					array(
						'type'     => 'checkbox',
						'prefix'   => '',
						'id'       => $this->get_field_id('img_placeholder'),
						'name'     => $this->get_field_name('img_placeholder'),
						'label'    => __( 'Show placeholder if no image is available?', 'connections_widgets' ),
						'before'   => '<p>',
						'after'    => '</p>',
						'layout'   => '%field% %label%',
						),
					isset( $instance['img_placeholder'] ) && $instance['img_placeholder'] !== '0' ? '1' : FALSE
				);

				if ( isset( $instance['img_placeholder'] ) && $instance['img_placeholder'] !== '0' ) {

					switch ( $instance['show'] ) {

						case 'logo':
							$placeholderString = isset( $instance['str_img_placeholder'] ) ? esc_attr( $instance['str_img_placeholder'] ) : __( 'No Logo', 'connections_widgets' );
							break;

						case 'photo':
							$placeholderString = isset( $instance['str_img_placeholder'] ) ? esc_attr( $instance['str_img_placeholder'] ) : __( 'No Photo', 'connections_widgets' );
							break;
					}

					cnHTML::input(
						array(
							'type'   => 'text',
							'prefix' => '',
							'class'  => 'widefat',
							'id'     => $this->get_field_id('str_img_placeholder'),
							'name'   => $this->get_field_name('str_img_placeholder'),
							'label'  => __( 'Image Placeholder Text:', 'connections_widgets' ),
							'before' => '<p>',
							'after'  => '</p>',
							),
						$placeholderString
					);

				}

			}

		}

		cnHTML::input(
			array(
				'type'   => 'text',
				'prefix' => '',
				'class'  => 'widefat',
				'id'     => $this->get_field_id('name_format'),
				'name'   => $this->get_field_name('name_format'),
				'label'  => __( 'Name format:', 'connections_widgets' ),
				'before' => '<p>',
				'after'  => '</p>',
				),
			isset( $instance['name_format'] ) && strlen( $instance['name_format'] ) > 0 ? esc_attr( $instance['name_format'] ) : '%prefix% %first% %middle% %last% %suffix%'
		);

		echo '<p class="description">' . __( sprintf( 'Valid tokens for the name format are: %s', '<br><code>%prefix%</code>, <code>%first%</code>, <code>%middle%</code>, <code>%last%</code>, <code>%suffix%</code>'), 'connections_widgets' ) . '</p>';

		echo '<p class="description">' . __( sprintf( 'Default value for name format is: %s', '<br><code>%prefix% %first% %middle% %last% %suffix%</code>'), 'connections_widgets' ) . '</p>';

		echo '<p class="description">' . __( '<strong>NOTE:</strong> The name format will not affect the entry types of Organization and Family.', 'connections_widgets' ) . '</p>';

		cnHTML::field(
			array(
				'type'     => 'checkbox',
				'prefix'   => '',
				'id'       => $this->get_field_id('link'),
				'name'     => $this->get_field_name('link'),
				'label'    => __( 'Link to entry?', 'connections_widgets' ),
				'before'   => '<p>',
				'after'    => '</p>',
				'layout'   => '%field% %label%',
				),
			isset( $instance['link'] ) && $instance['link'] !== '0' ? '1' : '0'
		);

		$display = isset( $instance['display_no_results'] ) && $instance['display_no_results'] !== '0' ? '1' : '0';

		if ( isset( $instance['display_no_results'] ) ) {

			$display = $instance['display_no_results'] !== '0' ? '1' : '0';

		} else {

			$display = '1';
		}

		cnHTML::field(
			array(
				'type'     => 'checkbox',
				'prefix'   => '',
				'id'       => $this->get_field_id('display_no_results'),
				'name'     => $this->get_field_name('display_no_results'),
				'label'    => __( 'Display widget when there are no upcoming anniversaries?', 'connections_widgets' ),
				'before'   => '<p>',
				'after'    => '</p>',
				'layout'   => '%field% %label%',
				),
			$display
		);

		if ( $display == '1' ) {

			cnHTML::input(
				array(
					'type'   => 'text',
					'prefix' => '',
					'class'  => 'widefat',
					'id'     => $this->get_field_id('no_results_message'),
					'name'   => $this->get_field_name('no_results_message'),
					'label'  => __( 'No result message:', 'connections_widgets' ),
					'before' => '<p>',
					'after'  => '</p>',
					),
				isset( $instance['no_results_message'] ) && strlen( $instance['no_results_message'] ) > 0 ? esc_attr( $instance['no_results_message'] ) : __( 'No Upcoming Anniversaries', 'connections_widgets' )
			);

		}

		$limit = isset( $instance['limit'] ) && filter_var( $instance['limit'], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) ) !== FALSE ? $instance['limit'] : 30;

		cnHTML::input(
			array(
				'type'   => 'number',
				'prefix' => '',
				'class'  => 'small-text',
				'id'     => $this->get_field_id('limit'),
				'name'   => $this->get_field_name('limit'),
				'label'  => __( 'The maximum number of days to look ahead for upcoming anniversaries:', 'connections_widgets' ),
				'before' => '<p>',
				'after'  => '</p>',
				'layout'   => '%label% %field%',
				),
			$limit
		);

		echo '<p class="description">' . __( '<strong>NOTE:</strong> The default is <code>30</code> days.', 'connections_widgets' ) . '</p>';

		// Add Chosen support for the enhanced select drop downs.
		wp_enqueue_style( 'cn-chosen' );
		wp_enqueue_script( 'jquery-chosen' );
		add_action( 'admin_print_footer_scripts' , array( 'Connections_Widgets', 'chosen' ) );

	}

	/**
	 * Callback to display the widget on the frontend.
	 *
	 * @access private
	 * @since  1.0
	 * @uses   Connections_Directory()
	 * @uses   cnTemplateFactory::loadTemplate()
	 * @uses   wp_get_current_user()
	 * @uses   cnFragment()
	 * @uses   cnRetrieve::entries()
	 * @uses   add_filter()
	 * @uses   remove_filter()
	 * @param array $atts
	 * @param array $instance
	 *
	 * @return void
	 */
	public function widget( $atts, $instance ) {

		// Grab an instance of the Connections object.
		$connections = Connections_Directory();

		/**
		 * @var $before_widget
		 * @var $show_title
		 *  Will only exist if the widget is being displayed via the `[cn_widget]` shortcode.
		 * @var $before_title
		 * @var $after_title
		 * @var $after_widget
		 */extract( $atts );

		if ( ! isset( $show_title ) ) $show_title = TRUE;

		$template = cnTemplateFactory::loadTemplate( array( 'template' => 'widget-upcoming-anniversaries' ) );

		/*
		 * --> START <--
		 * Setup the default widget options if they were not set when they were added to the sidebar;
		 * the user did not click the "Save" button on the widget.
		 */

		// Add the `widget_title` filter to match the WP core widgets.
		$title = apply_filters( 'widget_title', strlen( $instance['title'] ) > 0 ? $instance['title'] : __( 'Upcoming Anniversaries', 'connections_widgets' ), $instance, $this->id_base, $this );

		if ( isset( $instance['category'] ) && ! empty( $instance['category'] ) ) {

			$instance['category'] = json_decode( $instance['category'], TRUE );

			array_walk( $instance['category'], 'absint' );

		} else {

			$instance['category'] = array();
		}

		$category = isset( $instance['category_in'] ) && $instance['category_in'] !== '0' ? 'category_in' : 'category';

		if ( isset( $instance['exclude_category'] ) && ! empty( $instance['exclude_category'] ) ) {

			$instance['exclude_category'] = json_decode( $instance['exclude_category'], TRUE );

			array_walk( $instance['exclude_category'], 'absint' );

		} else {

			$instance['exclude_category'] = array();
		}

		$instance['show'] = isset( $instance['show'] ) && in_array( $instance['show'], array( 'none', 'date', 'logo', 'photo' ) ) ? $instance['show'] : 'date';

		$instance['img_size'] = isset( $instance['img_size'] ) ? absint( $instance['img_size'] ) : 48;

		$instance['img_style'] = isset( $instance['img_style'] ) && in_array( $instance['img_style'], array( 'circle', 'square' ) ) ? $instance['img_style'] : 'square';

		$instance['img_crop_mode'] = isset( $instance['img_crop_mode'] ) && in_array( $instance['img_crop_mode'], array( 0, 1, 2, 3 ) ) ? (int) $instance['img_crop_mode'] : 1;

		$instance['img_placeholder'] = isset( $instance['img_placeholder'] ) && $instance['img_placeholder'] !== '0' ? TRUE : FALSE;

		switch ( $instance['show'] ) {

			case 'logo':
				$instance['str_img_placeholder'] = isset( $instance['str_img_placeholder'] ) && strlen( $instance['str_img_placeholder'] ) > 0 ? esc_attr( $instance['str_img_placeholder'] ) : __( 'No Logo', 'connections_widgets' );
				break;

			case 'photo':
				$instance['str_img_placeholder'] = isset( $instance['str_img_placeholder'] ) && strlen( $instance['str_img_placeholder'] ) > 0 ? esc_attr( $instance['str_img_placeholder'] ) : __( 'No Photo', 'connections_widgets' );
				break;
		}

		$instance['name_format'] = isset( $instance['name_format'] ) && strlen( $instance['name_format'] ) > 0 ? $instance['name_format'] : '%prefix% %first% %middle% %last% %suffix%';

		$instance['link'] = ! isset( $instance['link'] ) || $instance['link'] !== '0' ? TRUE : FALSE;

		$instance['limit'] = isset( $instance['limit'] ) && filter_var( $instance['limit'], FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 0 ) ) ) !== FALSE ? $instance['limit'] : 0;

		$instance['display_no_results'] = ! isset( $instance['display_no_results'] ) || $instance['display_no_results'] !== '0' ? TRUE : FALSE;

		$message = isset( $instance['no_results_message'] ) && strlen( $instance['no_results_message'] ) > 0 ? esc_attr( $instance['no_results_message'] ) : __( 'No Anniversaries Today', 'connections_widgets' );

		/*
		 * --> END <--
		 * Setup the default widget options if they were not set when they were added to the sidebar;
		 * the user did not click the "Save" button on the widget.
		 */

		$user = wp_get_current_user();

		$key   = 'user-' . $user->ID;
		$group = 'cnw_upcoming_anniversaries-' . $this->number;

		$widget = new cnFragment( $key, $group );

		ob_start();

		if ( ! $widget->get() ) {

			$results = array();

			$ids = $connections->retrieve->upcoming(
				array(
					'type'                  => 'anniversary',
					'days'                  => empty( $instance['limit'] ) ? 30 : $instance['limit'],
					'today'                 => FALSE,
					'allow_public_override' => FALSE,
					'private_override'      => FALSE,
					'return'                => 'id',
				)
			);

			if ( ! empty( $ids ) ) {

				$results = $connections->retrieve->entries(
					array(
						'lock'             => TRUE,
						$category          => $instance['category'],
						'exclude_category' => $instance['exclude_category'],
						'id'               => $ids,
						'order_by'         => 'id|SPECIFIED',
					)
				);
			}

			if ( ! empty( $results ) ) {

				echo '<ul class="cn-widget cn-upcoming-anniversaries">';

				foreach ( $results as $row ) {

					$entry = new cnEntry_HTML( $row );
					$entry->directoryHome( array( 'force_home' => TRUE ) );

					echo '<li class="cat-item cat-item-' . $entry->getId() . ' cn-entry">';

						do_action( 'cn_template-' . $template->getSlug(), $entry, $template, $instance );

					echo '</li>';
				}

				echo '</ul>';

			}

			$widget->save( DAY_IN_SECONDS );
		}

		$out = ob_get_clean();

		$widget = compact( 'before_widget', 'show_title', 'before_title', 'title', 'after_title', 'after_widget' );

		if ( ! empty( $out ) ) {

			Connections_Widgets::render( $widget, $out );

		} elseif ( $instance['display_no_results'] ) {

			$out = '<ul class="cn-widget cn-upcoming-anniversaries">';
				$out .= '<li class="cat-item cn-entry"><span class="cn-widget cn-no-results">' . $message . '</span></li>';
			$out .= '</ul>';

			Connections_Widgets::render( $widget, $out );
		}
	}

}
