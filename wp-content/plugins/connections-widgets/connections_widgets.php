<?php
/**
 * An extension for the Connections Business Directory plugin which adds various widgets.
 *
 * @package   Connections Business Directory - Widget Pack
 * @category  Extension
 * @author    Steven A. Zahm
 * @license   GPL-2.0+
 * @link      http://connections-pro.com
 * @copyright 2018 Steven A. Zahm
 *
 * @wordpress-plugin
 * Plugin Name:       Connections Business Directory - Widget Pack
 * Plugin URI:        https://connections-pro.com/add-on/widget-pack/
 * Description:       An extension for the Connections Business Directory plugin which adds various widgets.
 * Version:           2.6.1
 * Author:            Steven A. Zahm
 * Author URI:        http://connections-pro.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       connections_widgets
 * Domain Path:       /languages
 *
 * @todo Fragment caches should be cleared when the URL permalink slugs are changed.
 */

if ( ! class_exists('Connections_Widgets') ) {

	final class Connections_Widgets {

		const VERSION = '2.6.1';

		/**
		 * @var string The absolute path this this file.
		 *
		 * @access private
		 * @since 2.4
		 */
		private static $file = '';

		/**
		 * @var string The URL to the plugin's folder.
		 *
		 * @access private
		 * @since 2.4
		 */
		private static $url = '';

		/**
		 * @var string The absolute path to this plugin's folder.
		 *
		 * @access private
		 * @since 2.4
		 */
		private static $path = '';

		/**
		 * @var string The basename of the plugin.
		 *
		 * @access private
		 * @since 2.4
		 */
		private static $basename = '';

		/**
		 * Stores the instance of this class.
		 *
		 * @access private
		 * @static
		 * @since  2.0
		 * @var    Connections_Widgets
		 */
		private static $instance;

		/**
		 * A dummy constructor to prevent the class from being loaded more than once.
		 *
		 * @access public
		 * @since  2.0
		 */
		public function __construct() { /* Do nothing here */ }

		/**
		 * The main plugin instance.
		 *
		 * @access  private
		 * @static
		 * @since  2.0
		 * @return object self
		 */
		public static function instance() {

			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connections_Widgets ) ) {

				self::$file       = __FILE__;
				self::$url        = plugin_dir_url( self::$file );
				self::$path       = plugin_dir_path( self::$file );
				self::$basename   = plugin_basename( self::$file );

				self::$instance = new Connections_Widgets;

				self::defineConstants();
				self::includes();

				// Add the CSS to the page head.
				//add_action( 'wp_head', array( __CLASS__, 'css') );
				add_action( 'admin_head', array( __CLASS__, 'adminCSS' ) );

				// Register the widgets.
				add_action( 'widgets_init', array( 'cnWidgetSearch', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetList', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetCategory', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetRecentlyAdded', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetRecentlyModified', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetUpcomingBirthdays', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetTodaysBirthdays', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetUpcomingAnniversaries', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetTodaysAnniversaries', 'register' ) );
				add_action( 'widgets_init', array( 'cnWidgetUpcomingEvents', 'register' ) );

				// Register the side bar to support widget shortcodes.
				add_action( 'widgets_init', array( __CLASS__, 'shortcodeSidebar'), 99 );

				// To ensure that the widgets in the shortcode sidebar are preserved when switching themes.
				add_action( 'switch_theme', array( __CLASS__, 'saveShortcodeSidebar' ) );
				add_action( 'after_switch_theme', array( __CLASS__, 'restoreShortcodeSidebar' ) );

				// Show the widget shortcode.
				add_action( 'in_widget_form', array( __CLASS__, 'widgetShortcode' ), 10, 3 );

				// Register the shortcode.
				add_shortcode( 'cn_widget', array( __CLASS__, 'shortcode' ) );

				// Add an action to purge widget fragment caches after adding/editing and entry.
				add_action( 'cn_clean_entry_cache', array( __CLASS__, 'clearFragments' ) );
				add_action( 'cn_clean_term_cache', array( __CLASS__, 'clearFragments' ) );

				// Add ajax action to delete fragment cache when a widget is deleted.
				add_action( 'wp_ajax_save-widget', array( __CLASS__, 'deleteFragment' ), -1 );

				// Beaver Builder plugin compatibility.
				add_action( 'fl_builder_before_render_ajax_layout', array( __CLASS__, 'clearFragments' ) );
				add_action( 'fl_builder_before_render_ajax_layout', array( 'cnTemplateFactory', 'activate' ) );

				// Elementor plugin compatibility. Not required. Templates only need loaded in admin context as well.
				//add_action( 'elementor/ajax/register_actions', array( __CLASS__, 'clearFragments' ) );
				//add_action( 'elementor/ajax/register_actions', array( 'cnTemplateFactory', 'activate' ) );

				// Register the scripts.
				add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueueScripts' ) );

				// License and Updater.
				if ( class_exists( 'cnLicense' ) ) {

					new cnLicense( __FILE__, 'Widget Pack', self::VERSION, 'Steven A. Zahm' );
				}
			}

			return self::$instance;
		}

		/**
		 * Define the constants.
		 *
		 * @access private
		 * @static
		 * @since 1.0
		 * @return void
		 */
		private static function defineConstants() {

			define('CNWD_DIR_NAME', plugin_basename( dirname( __FILE__ ) ) );
			define('CNWD_BASE_NAME', plugin_basename( __FILE__ ) );
			define('CNWD_BASE_PATH', WP_PLUGIN_DIR . '/' . CNWD_DIR_NAME);
			define('CNWD_BASE_URL', WP_PLUGIN_URL . '/' . CNWD_DIR_NAME);
		}

		/**
		 * Load the files required for the plugin.
		 *
		 * @access private
		 * @static
		 * @since  2.0
		 * @return void
		 */
		private static function includes() {

			// Require the widgets.
			require_once('includes/class.widget-list.php');
			require_once('includes/class.widget-category.php');
			require_once('includes/class.widget-recently-added.php');
			require_once('includes/class.widget-recently-modified.php');
			require_once('includes/class.widget-search.php');
			require_once('includes/class.widget-todays-anniversaries.php');
			require_once('includes/class.widget-todays-birthdays.php');
			require_once('includes/class.widget-upcoming-anniversaries.php');
			require_once('includes/class.widget-upcoming-birthdays.php');
			require_once('includes/class.widget-upcoming-list.php');

			/*
			 * Require the widget templates.
			 *
			 * NOTE: These need to be required in admin and frontend so the widgets work onm Elementor.
			 */
			require_once('templates/recently-added/recently-added.php');
			require_once('templates/recently-modified/recently-modified.php');
			require_once('templates/todays-anniversaries/todays_anniversaries.php');
			require_once('templates/todays-birthdays/todays_birthdays.php');
			require_once('templates/upcoming-anniversaries/upcoming_anniversaries.php');
			require_once('templates/upcoming-birthdays/upcoming_birthdays.php');
			require_once('templates/upcoming-list/upcoming_list.php');
		}

		/**
		 * Echoes the widget.
		 *
		 * @access public
		 * @since  2.0.1
		 *
		 * @param array  $widget
		 * @param string $html
		 */
		public static function render( $widget, $html ) {

			/**
			 * @var string $before_widget
			 * @var bool   $show_title
			 * @var string $before_title
			 * @var string $title
			 * @var string $after_title
			 * @var string $after_widget
			 */
			extract( $widget );

			echo $before_widget;

			if ( $show_title && strlen( $title ) > 0 ) {

				echo $before_title . $title . $after_title . PHP_EOL;
			}

			echo $html;

			echo $after_widget;
		}

		/**
		 * Registers the Widgets Shortcode sidebar.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @uses   register_sidebar()
		 * @return void
		 */
		public static function shortcodeSidebar() {

			register_sidebar( array(
				'name'          => __( 'Connections: Widget Shortcodes', 'connections_widgets' ),
				'id'            => 'cn_widget_shortcodes',
				'description'   => __( 'This widget area can be used for the &#91;cn_widget&#93; shortcode.', 'connections_widgets' ),
				'before_widget' => '',
				'after_widget'  => '',
			) );
		}

		/**
		 * Save the widgets we use for shortcodes as they will be "lost" when switching themes.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @return void
		 */
		public static function saveShortcodeSidebar() {

			$sidebars_widgets = wp_get_sidebars_widgets();

			if ( ! empty( $sidebars_widgets['cn_widget_shortcodes'] ) ) {

				update_option( 'cn_widget_shortcodes_saved', $sidebars_widgets['cn_widget_shortcodes'] );

			}

		}

		/**
		 * After switching themes, restore the widget shortcode.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @return void
		 */
		public static function restoreShortcodeSidebar() {

			$sidebars_widgets = wp_get_sidebars_widgets();

			if ( empty( $sidebars_widgets['cn_widget_shortcodes'] ) ) {

				$sidebars_widgets['cn_widget_shortcodes'] = get_option('cn_widget_shortcodes_saved');

				update_option( 'sidebars_widgets', $sidebars_widgets );
			}

		}

		/**
		 * Shows the shortcode for the widget.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 *
		 * @param $widget
		 * @param $return
		 * @param $instance
		 *
		 * @return void
		 */
		public static function widgetShortcode( $widget, $return, $instance ) {

			echo '<p class="cn_widget_shortcode" style="display: none">' . __( 'Shortcode', 'connections_widgets' ) . ':<br><code>' . ( ( $widget->number == '__i__' ) ? __( 'Please click "Save" to genreate the shortcode.', 'connections_widgets' ) : '[cn_widget id="'. $widget->id .'"]' ) . '</code></p>';
		}

		/**
		 * The shortcode callback for the `cn_widget` shortcode.
		 *
		 * @access public
		 * @since  2.0
		 * @static
		 * @global array  $wp_registered_widgets
		 * @global array  $wp_registered_sidebars
		 * @global array  $wp_widget_factory
		 * @uses   shortcode_atts()
		 * @uses   cnFormatting::toBoolean()
		 * @uses   get_option()
		 * @uses   self::parentSidebarID()
		 * @uses   apply_filters()
		 * @uses   do_action()
		 * @param  array  $atts    The shortcode attributes.
		 * @param  string $content [optional] The content from between a shortcode.
		 * @param  string $tag     [optional] The shortcode tag.
		 *
		 * @return string          The widget HTML.
		 */
		public static function shortcode( $atts, $content = '', $tag = 'cn_widget' ) {
			global $wp_registered_widgets, $wp_registered_sidebars, $wp_widget_factory;

			$defaults = array(
				'id'            => '',
				'title'         => TRUE,
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
				'after_widget'  => '</div>',
				'return'        => TRUE,
				);

			$atts = shortcode_atts( $defaults, $atts, $tag );

			/*
			 * Convert some of the $atts values in the array to boolean.
			 */
			cnFormatting::toBoolean( $atts['return'] );

			// Whether or not to show the widget title.
			$show_title = ( '0' == $atts['title'] || '' == $atts['title'] ) ? FALSE : TRUE;

			// Bail if no widget ID was supplied or an incorrect/invalid widget ID was supplied.
			if ( empty( $atts['id'] ) || ! isset( $wp_registered_widgets[ $atts['id'] ] ) ) {

				return '';
			}

			// Get the widget instance ID number.
			$id     = explode( '-', $atts[ 'id' ] );
			$number = array_pop( $id ); //var_dump($number);

			// Get the widget's options, all instances.
			$options = get_option( $wp_registered_widgets[ $atts['id'] ]['callback'][0]->option_name ); //var_dump($options);

			// Get this instance widget's options.
			$instance = $options[ $number ];

			// Get the class name of the widget.
			if ( ! $class = get_class( $wp_registered_widgets[ $atts['id'] ]['callback'][0] ) ) {

				// Maybe the widget is removed or unregistered.
				return '';
			}

			// Get the widget's assigned sidebar ID.
			$sidebar = self::parentSidebarID( $atts['id'] ); //var_dump( $sidebar );

			if ( 'wp_inactive_widgets' === $sidebar ) {

				return '<div class="cnw-widget-id-inactive">' . sprintf( __( 'Widget %s ID found but it is inactive.', 'connections_widgets' ), $atts['id'] ) . '</div>';
			}

			/* The widget params that must be run thru the `dynamic_sidebar_params` filter. */
			$params = array(
				0 => array(
					'name'          => $wp_registered_sidebars[ $sidebar ]['name'],
					'id'            => $wp_registered_sidebars[ $sidebar ]['id'],
					'description'   => $wp_registered_sidebars[ $sidebar ]['description'],
					'before_widget' => sprintf( $atts['before_widget'], $atts['id'], $wp_registered_widgets[ $atts['id'] ]['classname'] ),
					'show_title'    => TRUE,
					'before_title'  => $atts['before_title'],
					'after_title'   => $atts['after_title'],
					'after_widget'  => $atts['after_widget'],
					'widget_id'     => $atts['id'],
					'widget_name'   => $wp_registered_widgets[ $atts['id'] ]['name']
				),
				1 => array(
					'number'        => $number
				)
			);

			// Whether or not to display the widget title. If it is to be rendered, use a custom title?
			if ( ! $show_title || strlen( $atts['title'] ) == 0 ) {

				$params[0]['show_title']   = FALSE;

				$params[0]['before_title'] = '';
				$params[0]['after_title']  = '';

				$instance['title']         = '';

			} elseif ( is_string( $atts['title'] ) && strlen( $atts['title'] ) > 0 ) {

				$instance['title'] = $atts['title'];

			}

			$params = apply_filters( 'dynamic_sidebar_params', $params );

			// Render the widget.
			ob_start();

			// We're not going to use this because it sets the widget instance ID to -1 which screws with the fragment caching.
			// the_widget( $class, $instance, $params[0] );

			// Instead we're going to directly grab the widget object.
			// Code lifted from WP core, function `the_widget()` in the file ../wp-includes/widgets.php.
			/** @var $object WP_Widget */
			$object = $wp_widget_factory->widgets[ $class ];

			if ( ! is_a( $object, 'WP_Widget') ) {

				return '';
			}

			/**
			 * Fires before rendering the requested widget.
			 *
			 * @param string $class    The widget's class name.
			 * @param array  $instance The current widget instance's settings.
			 * @param array  $args     An array of the widget's sidebar arguments.
			 */
			do_action( 'the_widget', $class, $instance, $params[0] );

			$object->_set( $number );

			// Run the widget callback to display the widget.
			$object->widget( $params[0], $instance );

			$out = ob_get_clean();

			if ( $atts['return'] ) return $out;

			echo $out;
		}

		/**
		 * Returns the parent sidebar ID of the supplied widget instance ID.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @uses   wp_get_sidebars_widgets()
		 * @param  string $widget_id The widget instance id.
		 * @return mixed bool | string FALSE if the sidebar ID can not be found with the supplied widget ID || the sidebar ID.
		 */
		private static function parentSidebarID( $widget_id ) {

			$sidebars_widgets = wp_get_sidebars_widgets();

			if ( ! empty( $sidebars_widgets ) ) {

				foreach( $sidebars_widgets as $sidebar => $widgets ) {

					if ( ! empty( $widgets ) ) {

						foreach( $widgets as $widget ) {

							if ( $widget_id == $widget ) return $sidebar;
						}
					}
				}
			}

			return FALSE;
		}

		/**
		 * Purge widget fragment caches after adding/editing and entry.
		 *
		 * @access public
		 * @since  2.0
		 * @static
		 * @uses   cnCache::clear()
		 * @return void
		 */
		public static function clearFragments() {

			cnCache::clear( TRUE, 'transient', 'cnw' );
		}

		/**
		 * Purge the widget group fragment cache when a widget is deleted.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @uses   check_ajax_referer()
		 * @uses   wp_die()
		 * @uses   cnFragment::clear();
		 * @return void
		 */
		public static function deleteFragment() {

			check_ajax_referer( 'save-sidebar-widgets', 'savewidgets' );

			if ( ! current_user_can('edit_theme_options') || ! isset( $_POST['id_base'] ) ) {

				wp_die( -1 );
			}

			if ( isset( $_POST['delete_widget'] ) && $_POST['delete_widget'] ) {

				// Clear the widget group fragment cache.
				cnFragment::clear( TRUE, substr( $_POST['widget-id'], 0 ) );
			}

		}

		/**
		 * CSS for the widget shortcode sidebar.
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 *
		 * @return string
		 */
		public static function adminCSS() {
			global $pagenow;

			if ( $pagenow == 'widgets.php' ) {

				echo '<!-- Connections Widget Pack CSS -->';
				echo '<style>#cn_widget_shortcodes .cn_widget_shortcode { display: block !important; }</style>';
			}
		}

		/**
		 * Enqueue the scripts.
		 *
		 * @access private
		 * @since  2.4
		 * @static
		 */
		public static function enqueueScripts() {

			// If SCRIPT_DEBUG is set and TRUE load the non-minified JS files, otherwise, load the minified files.
			$min = defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ? '' : '.min';
			$url = cnURL::makeProtocolRelative( self::$url );

			wp_enqueue_style( 'cn-widgets', $url . "assets/css/cn-widgets$min.css" );

			wp_register_script( 'cn-widgets-js', $url . "assets/js/cn-widgets$min.js", array( 'jquery' ), self::VERSION, TRUE );
		}

		/**
		 * Output the JS necessary to support Chosen.
		 * @url https://make.wordpress.org/core/2014/04/17/live-widget-previews-widget-management-in-the-customizer-in-wordpress-3-9/
		 *
		 * @access private
		 * @since  2.0
		 * @static
		 * @return void
		 */
		public static function chosen() {

?>

<script type="text/javascript">/* <![CDATA[ */

// ;jQuery(document).ready( function($) {

// 	// Add jQuery Chosen to enhanced select drop down fields.
// 	if ( $.fn.chosen ) {

// 		$( '.cn-enhanced-select' ).chosen( { width: '100%' } );

// 	}
// });

;jQuery( function($) {

	function init( widget_el, is_cloned ) {

		// Clean up from the clone which lost all events and data
		if ( is_cloned ) {

			widget_el.find( '.chosen-container' ).remove();
		}

		widget_el.find( '.cn-enhanced-select' ).chosen( { width: '100%' } );
	}

	function cn_widget_update( e, widget_el ) {

		// if ( 'dynamic_fields_test' === widget_el.find( 'input[name="id_base"]' ).val() ) {

			init( widget_el, 'widget-added' === e.type );
		// }
	}

	$( document ).on( 'widget-updated', cn_widget_update );
	$( document ).on( 'widget-added', cn_widget_update );

	$( '.widget:has(.cn-enhanced-select)' ).each( function() {

		init( $( this ) );
	});

});
/* ]]> */</script>

<?php

		}

	}

	/**
	 * Start up the extension.
	 *
	 * @access public
	 * @since  2.0
	 * @return mixed (object)|(bool)
	 */
	function Connections_Widgets() {

		if ( class_exists('connectionsLoad') ) {

			return Connections_Widgets::instance();

		} else {

			add_action(
				'admin_notices',
				 create_function(
					'',
					'echo \'<div id="message" class="error"><p><strong>ERROR:</strong> Connections must be installed and active in order use Connections Widget Pack.</p></div>\';'
					)
			);

			return FALSE;
		}
	}

	/**
	 * We'll load the extension on `plugins_loaded` so we know Connections will be loaded and ready first.
	 */
	add_action( 'plugins_loaded', 'Connections_Widgets' );
}
