<?php
/*
Plugin Name: Leede Jones Clients Pages plugin
Plugin URI: 
Description: Add Custom Pages for clients
Version: 1.0
Author: John Anderson/Graphically Speaking
Author URI: http://www.graphicallyspeaking.ca
License: GPL2
*/
/*
Copyright 2019  Graphically Speaking  (email : john.anderson@graphicallyspeaking.ca)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
 
// Hook <strong>lc_custom_post_movie()</strong> to the init action hook
add_action( 'init', 'lj_custom_page_clients' );
 
// The custom function to register a movie post type
function lj_custom_page_clients() 
{
 
  // Set the labels, this variable is used in the $args array
  $labels = array(
    'name'               => __( 'Client Pages' ),
    'singular_name'      => __( 'Client Page' ),
    'add_new'            => __( 'Add New Client Page' ),
    'add_new_item'       => __( 'Add New Client Page' ),
    'edit_item'          => __( 'Edit Client Page' ),
    'new_item'           => __( 'New Client Page' ),
    'all_items'          => __( 'All Client Pages' ),
    'view_item'          => __( 'View Client Page' ),
    'search_items'       => __( 'Search Client Pages' )
  );
 
  // The arguments for our post type, to be entered as parameter 2 of register_post_type()
  $args = array(
    'labels'            => $labels,
    'description'       => 'Holds client pages',
    'public'            => true,
    'menu_position'     => 10,
    //'supports'          => array( 'custom-fields' ),
    'has_archive'       => true,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'has_archive'       => true,
    'capability_type'   => 'page',
    'menu_icon'         => 'dashicons-admin-page'
  );
 
  // Call the actual WordPress function
  // Parameter 1 is a name for the post type
  // Parameter 2 is the $args array
  register_post_type( 'client_pages', $args);
}