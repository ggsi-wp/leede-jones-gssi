<?php
/**
 * Broadridge class containing required functionality
 */
class Broadridge {

    function __construct()
    {
        $this->url  = [
            "https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iaxml_x.p?user=",
            "&pass="
        ];
        $this->loginUrl = "https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/ialognx.p?sess=";
    }

    function broadridgeLogin( $user, $pass )
    {   
        $url    = $this->url[0] . $user . $this->url[1] . $pass;
        $xml    = simplexml_load_file( $url );

        $sessionId  = urlencode( reset( $xml->SESSIONID ) ); 

        if( empty( $sessionId ) ) return FALSE;

        return $this->loginUrl . $sessionId;
    }
}