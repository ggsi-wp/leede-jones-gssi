<?php
/**
 * Doxim class containing required functionality
 */
class Doxim {
    public $_bin;
    public $_pass;
    public $_uri;
    
    public function __construct( $bin, $pass, $uri )
    {
        $this->_bin = $bin;
        $this->_pass= $pass;
        $this->_uri = $uri;
        $this->context  = $this->setContext();
    }

    /**
     * return results from XML call, using account(s) and based on type
     */
    public function getResults( $accounts, $type, $dateStr, $viewId, $minCount )
    {    
        $o  = [];
        foreach( $accounts as $acct )
        {
            if( empty( $acct ) ) continue;
            $xml    = $this->fetchXML( $acct, $viewId, $dateStr ); //var_dump( $xml );
            if( $xml )
            {
                $docUrl = $viewId . '_' . $acct . '_';
                $o      = array_merge( $o, $this->organiseData( $xml, $docUrl, $type ) ); 
            }
        }  
        // need to sort the results by date
        usort( $o, ["Doxim","sortByTime"] ); 

        // Is there a minCount?
        if( $minCount > 0 ) $o  = array_slice( $o, 0, $minCount );

        return $o;
    }


    /**
     * Fetch XML results by account and viewId
     */
    public function fetchXML( $acct, $viewId, $dateStr )
    {
        $apiGetDocs = $this->_uri . "bin=" . $this->_bin . '&passwd=' . $this->_pass . "&viewid=" . $viewId . "&mid=" . $acct . $dateStr; //var_dump( $apiGetDocs ); 

        if ( ( $response_xml_data = file_get_contents( $apiGetDocs, false, $this->context ) ) === false )
        {
            echo "Error fetching XML\n";
        } 
        else 
        {
            libxml_use_internal_errors(true);
            $xml    = simplexml_load_string($response_xml_data);
    /*        if (!$data) 
            {
                echo "Error loading XML\n";
                foreach(libxml_get_errors() as $error) 
                {
                    echo "\t", $error->message;
                }
            } 
            else 
            { 
                $docUrl = $viewId . '_' . $acct . '_';
                $o      = array_merge( $o, $this->organiseData( $data, $docUrl ) );
            }*/
        } 
        return $xml;    
    }


    public function setContext()
    {

        $dargs=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false),
             "http"=>array(
                 'timeout' => 60, 
                 'user_agent' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/3.0.0.1'
            )
        );
   
        $context    = stream_context_create( $dargs );

        return $context;

    }

    /**
     * Organise the XML data 
     */
    function organiseData( $xml, $docUrl, $type )
    {   
        $o  = [];
        foreach( $xml->rows->row as $a )
        {
            $a['docUrl']    = $docUrl;
            $a['timestamp'] = $this->retrieveTimestamp( $a, $type );
            $o[] = $a;
        } 
        return $o;
    }


    /**
     * Get the date for each row, and return a timestamp integer
     */
    function retrieveTimestamp( $a, $type )
    {
        return strtotime( $a->column[3] );
    }

    /**
     * Timestamp comparison function
     */
    private static function sortByTime( $a, $b )
    {
        if( $a['timestamp'] == $b['timestamp'] ) return 0;
        return ( intval(  $a['timestamp'] ) <  intval( $b['timestamp'] ) ) ? 1 : -1;
    }


}