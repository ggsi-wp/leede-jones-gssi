<?php
/*
Plugin Name: Leede Jones Clients plugin
Plugin URI: 
Description: Add post type for clients
Version: 1.0
Author: John Anderson/Graphically Speaking
Author URI: http://www.graphicallyspeaking.ca
License: GPL2
*/
/*
Copyright 2019  Graphically Speaking  (email : john.anderson@graphicallyspeaking.ca)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as 
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


define( 'INCLUDES', './wp-content/plugins/lj-clients/includes/' );
define( 'DOXIMFORM', './wp-content/plugins/lj-clients/templates/partials/doxim/' ); 
define( 'SECURITYFORM', './wp-content/plugins/lj-clients/templates/partials/security/' );
 
// Hook <strong>lc_custom_post_movie()</strong> to the init action hook
add_action( 'init', 'lj_custom_post_clients' );
 
// The custom function to register a movie post type
function lj_custom_post_clients() 
{
 
  // Set the labels, this variable is used in the $args array
  $labels = array(
    'name'               => __( 'Clients' ),
    'singular_name'      => __( 'Client' ),
    'add_new'            => __( 'Add New Client' ),
    'add_new_item'       => __( 'Add New Client' ),
    'edit_item'          => __( 'Edit Client' ),
    'new_item'           => __( 'New Client' ),
    'all_items'          => __( 'All Clients' ),
    'view_item'          => __( 'View Client' ),
    'search_items'       => __( 'Search Clients' )
  );
 
  // The arguments for our post type, to be entered as parameter 2 of register_post_type()
  $args = array(
    'labels'            => $labels,
    'description'       => 'Holds clients and related details',
    'public'            => true,
    'menu_position'     => 65,
    'supports'          => array( 'custom-fields' ),
    'has_archive'       => true,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'has_archive'       => true,
    'menu_icon'         => 'dashicons-admin-users'
  );
 
  // Call the actual WordPress function
  // Parameter 1 is a name for the post type
  // Parameter 2 is the $args array
  register_post_type( 'client', $args);
}

include_once( 'lj-clients-metaboxes.php' );
include_once( 'lj-clients-functions.php' );
include_once( 'lj-clients-pages.php' );
include_once( 'lj-clients-shortcodes.php' );
//include_once( 'lj-clients-widgets.php' );

wp_register_style( 'lj-styles', site_url() . '/wp-content/plugins/lj-clients/templates/css/client.css' );
wp_enqueue_style( 'lj-styles' );

add_action( 'save_post', 'jl1240_save_meta_box' );
add_filter( 'wp_insert_post_empty_content', 'jl1240_save_user_data' );
add_action( 'admin_enqueue_scripts', 'my_admin_enqueue_scripts' );
add_filter( 'post_updated_messages', 'post_published' );
add_action( 'admin_init', 'ui_new_role' );
add_filter( 'manage_client_posts_columns', 'lj1240_custom_columns' );
add_action( 'manage_client_posts_custom_column', 'lj1240_custom_columns_content', 10, 2 );
register_activation_hook( __FILE__, 'add_custom_pages' );
register_deactivation_hook( __FILE__, 'remove_custom_pages' );

add_action( 'admin_menu', 'my_plugin_menu' );

add_filter( 'wp_nav_menu_items', 'amend_menus', 10, 2 );


function my_plugin_menu()
{
    // Add Menu items
    add_submenu_page( 'options-general.php', 'Leede Jones Client parameters', 'Client parameters', 'manage_options', 'client_settings', 'my_plugin_options' );
    add_submenu_page( 'options-general.php', 'Leede Jones Doxim parameters', 'Doxim parameters', 'manage_options', 'doxim_settings', 'doxim_plugin_options' );

    // Call register settings function
    add_action( 'admin_init', 'register_client_settings' );
    add_action( 'admin_init', 'register_doxim_settings' );
}

function register_client_settings()
{
    $questions  = getQuestions();
    foreach( $questions as $k => $q )
    {
        register_setting( 'client-settings-group', 'security_questions_' . ( $k + 1 ) );
    }
    register_setting( 'client-settings-group', 'security_questions_' . ( count( $questions ) + 1 ) );
    register_setting( 'client-settings-group', 'security_questions_required' );
}

function register_doxim_setting()
{
    register_setting( 'lj_doxim-group', 'doxim_bin' );
    register_setting( 'lj_doxim-group', 'doxim_password' );
    register_setting( 'lj_doxim-group', 'doxim_apiurl' );
}

function my_plugin_options()
{
    if( !current_user_can( 'manage_options' ) )
    {
        wp_die( __( 'You do not have sufficient permissions to access this page' ) );
    }
    ?>

    <div class="wrap">
        <form method="post" action="options.php">
        <?php settings_fields( 'client-settings-group' ); ?>
        <?php do_settings_sections( 'client-settings-group' ); ?>
        <table class="form-table">
            <?php 
                $questions  = getQuestions();
                if( empty( $questions) ) $questions = [];
                foreach( $questions as $k => $q ): ?>
                    <tr valign="top">
                        <th scope="row">Security Question <?php echo $k + 1; ?></th>
                        <td><input type="text" name="security_questions_<?php echo $k + 1; ?>" value="<?php echo $q; ?>" size=45/></td>
                    </tr>
            <?php endforeach; ?>
            <tr valign="top">
                <th scope="row">Security Question <?php echo count( $questions ) + 1; ?></th>
                <td><input type="text" name="security_questions_<?php echo count( $questions) + 1; ?>" value="" size=45 /> New Security Question</td>
            </tr>            

            <tr valign="top">
                <th scope="row">Number of Security Questions required</th>
                <td><input type="text" name="security_questions_required" value="<?php echo esc_attr( get_option('security_questions_required') ); ?>" /></td>
            </tr>
        </table>
    
    <?php submit_button(); ?>

        </form>
    </div>
<?php
}

function doxim_plugin_options()
    {
        if( !current_user_can( 'manage_options' ) )
        {
            wp_die( __( 'You do not have sufficient permissions to access this page' ) );
        }
        ?>
    
        <div class="wrap">
            <form method="post" action="options.php">
            <?php settings_fields( 'lj_doxim-group' ); ?>
            <?php do_settings_sections( 'lj_doxim-group' ); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Bin number</th>
                    <td><input type="text" name="doxim_bin" value="<?php echo esc_attr( get_option('doxim_bin') ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Doxim Password</th>
                    <td><input type="password" name="doxim_password" value="<?php echo esc_attr( get_option('doxim_password') ); ?>" /></td>
                </tr>  
                <tr valign="top">
                    <th scope="row">Doxim apiUrl</th>
                    <td><input type="text" name="doxim_apiurl" value="<?php echo esc_attr( get_option('doxim_apiurl') ); ?>" /></td>
                </tr>    
            </table>
        
        <?php submit_button(); ?>
    
            </form>
        </div>
    <?php
    }    


/**
 * Set custom columns on Admin presentation
 */
function lj1240_custom_columns( $columns )
{
    $columns    = [
        'cb'        => $columns['cb'],
        'username'  => __( 'Username'),
        'firstname' => __( 'First Name' ),
        'lastname'  => __( 'Last Name' ),
        'advisor'   => __( 'Advisor' ),
        'agreement' => __( 'Accepted Agreement'),
        'account'   => __( 'Account Locked' ),
        'login'     => __( 'Last Logged In' )
    ];

    return $columns;
}

/**
 * Display custom fields on Client list page
 */
function lj1240_custom_columns_content( $column, $post_id )
{
    switch( $column )
    {
        case 'username':
            echo get_post_meta( $post_id, 'client_email', true );
            break;
        case 'firstname':
            echo get_post_meta( $post_id, 'client_firstname', true );
            break;
        case 'lastname':
            echo get_post_meta( $post_id, 'client_lastname', true );
            break; 
        case 'advisor':
            echo fetchAdvisor( $post_id );
            break;
        case 'agreement':
            echo get_post_meta( $post_id, 'client_lj_agreement', true ) ? 'Yes' : 'No';
            break;                        

    }
}


/**
 * Create new role for Clients
 */
function ui_new_role() {  
 
    //add the new user role
    add_role(
        'client',
        'Client',
        array(
            'read'         => true,
            'delete_posts' => false
        )
    );
}

/**
 * Suppress Post Updated message
 */
function post_published( $messages )
{
    unset( $messages['post'][6] );
    return $messages;
}

function my_admin_enqueue_scripts() {
    if ( 'client' == get_post_type() )
        wp_dequeue_script( 'autosave' );
}


/**
 * Test required User data
 */
function jl1240_save_user_data()
{
    global $post;

    if( isset( $_POST['post_type'] ) && 'client' != $_POST['post_type'] ) return FALSE;

    if( !isset( $post ) ) return ''; 
    
    if( isset( $_POST['client_email']  ) && !filter_var( $_POST['client_email'], FILTER_VALIDATE_EMAIL ) )
    {   
        SetError( 'Client Email is not a valid email address' );
        return TRUE;
    }
    if( isset( $_POST['client_email']  ) && empty( sanitize_text_field( $_POST['client_email'] ) ) )
    {
        setError( 'Client Email is required' );
        return TRUE;
    }
    if( isset( $_POST['client_email'] ) && email_exists( sanitize_text_field( $_POST['client_email'] ) ) && (!isset( $_POST['action'] ) ) && 'edit' != $_POST['action'] )
    {   
        setError( 'Client Email is already registered' );
        return TRUE;
    }
    if( 'auto-draft' == $post->post_status || 'draft' == $post->post_status )
    { 
        if( !isset( $_POST['client_new_pass'] ) || empty( sanitize_text_field( $_POST['client_new_pass'] ) ) )
        {
            setError( 'Client Password is required' );
            return TRUE;
        }
        elseif( sanitize_text_field( $_POST['client_new_pass'] ) !== sanitize_text_field( $_POST['client_new_pass_conf'] ) )
        {
            setError( 'New Password must match Confirm New Password' );
            return TRUE;
        }
    }
    elseif( isset( $_POST['client_new_pass'] ) && !empty( $pass = sanitize_text_field( $_POST['client_new_pass'] ) ) && $pass !== sanitize_text_field( $_POST['client_new_pass_conf'] )  )
    {
        setError( 'New Password must match Confirm New Password' );
        return TRUE;        
    }

    return FALSE;
}

/**
 * Create Client user
 */
function createClient()
{
    if( empty( $_POST ) ) return '';
    $username   = sanitize_text_field( $_POST['client_email'] );
    $password   = sanitize_text_field( $_POST['client_new_pass'] );
    
    $user_id        = wp_create_user( $username, $password, $username );
    $user           = new WP_User( $user_id );
    $user->set_role( 'client' );
    return $user_id;  
}

/**
 * Test for existence of lj-client-restriction shortcode, 
 * Redirect to login if user not logged in, or not Client
 */
function pre_process_shortcode()
{   
    if( !is_singular() ) return;
    global $post; 
    if( !empty( $post->post_content ) )
    {
        $regex  = get_shortcode_regex(); 
        preg_match_all( '/'.$regex.'/', $post->post_content,$matches );
        
        if( !empty( $matches[2] && in_array( 'lj-client-restriction', $matches[2] ) ) )
        {   
            if( !is_user_logged_in() )
            {
//                wp_redirect( site_url(). '/login' );
                //CUSTOM
                wp_redirect( leede_getTplPageURL( 'templates/page-login.php' ) );
            }
            $userdata   = get_userdata( get_current_user_id() ); 
            if( !in_array( 'client', $userdata->roles ) )
            {
                wp_redirect( get_dashboard_url() );
            }
            $clientId   = getClientId( $userdata->ID );
            $pwchange   = get_post_meta( $clientId, 'client_password_change', TRUE ); 
            if( $pwchange && FALSE === strpos( $_SERVER['REQUEST_URI'], 'change-password' ) )
            {
                wp_redirect( site_url() . '/change-password' );
            }
            
            //wp_redirect( site_url(). '/client-dashboard' );
        }
        if( !empty( $matches[2] && in_array( 'wpum_login_form', $matches[2] ) ) && is_user_logged_in() )
        {   
            $userdata   = get_userdata( get_current_user_id() ); 
            if( !in_array( 'client', $userdata->roles ) )
            {
                wp_redirect( get_dashboard_url() );
            }
            else
            {   
                wp_redirect( site_url() . '/client-dashboard' );
            }                
        }
    }
}

add_action( 'template_redirect', 'pre_process_shortcode', 1 );

/**
 * Test to redirect from already-logged-in page to either client dashboard or WP dashboard
 */
function alreadyLoggedIn()
{
    if( !is_singular() ) return;
    if( !is_user_logged_in() ) return;
    if( FALSE !== strpos( $_SERVER['REQUEST_URI'], 'password-reset' ) )
    {
        $userdata   = get_userdata( get_current_user_id() );
        if( !in_array( 'client', $userdata->roles ) )
        {
            wp_redirect( get_dashboard_url() );
        }
        wp_redirect( site_url() . '/client-dashboard' );
    }
}

add_action( 'template_redirect', 'alreadyLoggedIn', 1 );

/**
 * Test to redirect from wp-admin if logged in user is Client
 */
function restrictAdmin()
{   
    if( FALSE !== strpos( $_SERVER['REQUEST_URI'], 'wp-admin' ) )
    {   
        $userdata   = get_userdata( get_current_user_id() ); 
        if( in_array( 'client', $userdata->roles ) )
        {   
            wp_redirect( site_url() . '/client-dashboard' );
        }  
        return;      
    }
    return;
}
add_action( 'admin_init', 'restrictAdmin' );

/**
 * Hide Custom Fields when Clients Custom Post Type form is open
 */
add_action('admin_init', 'set_user_metaboxes');
function set_user_metaboxes($user_id=NULL) {

    // These are the metakeys we will need to update
    $meta_key['order'] = 'meta-box-order_post';
    $meta_key['hidden'] = 'metaboxhidden_post';

    // So this can be used without hooking into user_register
    if ( ! $user_id)
        $user_id = get_current_user_id(); 

    // Set the default order if it has not been set yet
    if ( ! get_user_meta( $user_id, $meta_key['order'], true) ) {
        $meta_value = array(
            'side' => 'submitdiv,formatdiv,categorydiv,postimagediv',
            'normal' => 'postexcerpt,tagsdiv-post_tag,postcustom,commentstatusdiv,commentsdiv,trackbacksdiv,slugdiv,authordiv,revisionsdiv',
            'advanced' => '',
        );
        update_user_meta( $user_id, $meta_key['order'], $meta_value );
    }

    // Set the default hiddens if it has not been set yet
    if ( ! get_user_meta( $user_id, $meta_key['hidden'], true) ) {
        $meta_value = array('postcustom','trackbacksdiv','commentstatusdiv','commentsdiv','slugdiv','authordiv','revisionsdiv');
        update_user_meta( $user_id, $meta_key['hidden'], $meta_value );
    }
}


/**
 * Logout without confirmation
 */

function logout_without_confirm($action, $result)
{
    if ( $action == 'log-out' && !isset($_GET['_wpnonce'] ) ) 
    {   
//        $redirect   = wp_logout_url( get_permalink( get_page_by_path( 'Login' ) ) );
        $redirect   = wp_logout_url( leede_getTplPageURL( 'templates/page-login.php' ) );
        $location = str_replace('&amp;', '&', $redirect );
        header( "Location: $location" );
        die;
    }
}
add_action( 'check_admin_referer', 'logout_without_confirm', 10, 2 );
