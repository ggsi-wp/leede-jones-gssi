<?php
/**
 * Template for Client Dashboard
 */

function dashboard()
{
    $userId = get_current_user_id();
    // Need to get clientId where 'client_user_id' == $userId;
    $clientId   = getClientId( $userId );
    //$broadridge = esc_attr( get_post_meta( $clientId, 'client_broadridge', TRUE ) );
//    $doxim      = esc_attr( get_post_meta( $clientId, 'client_doxim', TRUE ) );
	//CUSTOM DOXIM
	$doxim      = !empty(leede_get_account_number( get_user_meta( get_post_meta( $clientId, 'client_user_id', TRUE ), 'client_code', TRUE ) )) ? 1 : 0;
    ob_start();
    ?>

        <!--<h2>Client Dashboard</h2>-->
        <?php //include_once( 'partials/advisor.php' ); ?>

        <?php include_once( 'partials/welcome.php' ); ?>
        <?php //include_once( 'partials/dashboard-posts.php' ); ?>
        <?php //include_once( 'partials/doc-navigation.php' );
    $o  = ob_get_clean();

    return $o;
}