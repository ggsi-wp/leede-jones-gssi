<?php
/**
 * Welcome statement
 */
$firstname  = get_post_meta( $_SESSION['clientId'], 'client_firstname', TRUE );
$lastname   = get_post_meta( $_SESSION['clientId'], 'client_lastname', TRUE );
//$name       = $firstname .  ' ' . $lastname;
//$doxim      = get_post_meta( $_SESSION['clientId'], 'client_doxim', TRUE );
//CUSTOM DOXIM
$doxim  = !empty(leede_get_account_number( get_user_meta( get_post_meta( $_SESSION['clientId'], 'client_user_id', TRUE ), 'client_code', TRUE ) )) ? 1 : 0;
?>
<div class="welcome">
    <div class="welcome-wrapper">
        <?php if( !empty( $doxim ) ):?>
            <article>
                <div class="welcome_roundel"><img src="<?php echo site_url(); ?>/wp-content/plugins/lj-clients/templates/assets/documents.png" /></div>
                <a href="<?php echo site_url(); ?>/account-statements">
                <h3>Documents</h3>
                </a>
                <?php wp_nav_menu( ['menu' => 'Client Centre']  ); ?>
            </article>
        <?php endif; ?>
        <?php if( TRUE === $_SESSION['broadridge'] ): ?>
            <article>
                <div class="welcome_roundel"><img src="<?php echo site_url(); ?>/wp-content/plugins/lj-clients/templates/assets/accounts.png" /></div>
                <a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/holdings/holdaccts.htm">
                <h3>Account Status</h3>
                </a>
	            <?php wp_nav_menu( ['menu' => 'Account Status']  ); ?>
<!--                <ul>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/holdings/holdaccts.htm">Holdings</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/portfolios/portaccts.htm">Portfolio View</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/activity/actaccts.htm">Activity</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/orderbook/obaccts.htm">Pending Trades</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/activity/history.htm">History</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/plan/planaccts.htm">Plan Data</a></li>-->
<!--                    <li><a href="https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=/other/nicknames.htm">Nicknames</a></li>-->
<!--                </ul>-->
            </article>   
        <?php endif; ?>
        <?php echo do_shortcode('[leede_manage_profile_sidebar]')?>
    </div>
</div>