<?php
    function result( $results )
    {   
        ob_start();
        if( isset( $results ) ): 
            if( count( $results ) > 0 ): 
            ?>
                <table class="lj-results-table">
                    <!--<colgroup>
						<col class="result-account-name">
                        <col class="result-account">
                        <col class="result-date">
                    </colgroup>
					-->
                    <thead>
                        <tr>
							<td>Account Name</td>
                            <td>Account #</td>
                            <td>Period Ending Date</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
						//var_dump($results); die;
                            foreach( $results as $row ): 
							//var_dump( $row ); die;
                                $str    = $row['docUrl'] . $row->attributes()->id;//$row['docUrl'] . '&docid=' . $row->attributes()->id;
                                $estr   = my_simple_crypt( $str, 'e' );
                                $link   = site_url() . '/client-download?docid=' . $estr;
                            ?>
                                <tr>
									<td><?php echo $row->column[7]; ?></td>
                                    <td><a href="<?php echo $link; ?>" target="_blank"><?php echo $row->column[4]; ?></a></td>
                                    <td><a href="<?php echo $link; ?>" target="_blank"><?php echo date( 'M j, Y', strtotime( $row->column[3] ) ); ?></a></td>
                                </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
        <?php else: ?>
            <p>No results</p>
        <?php endif; 
        endif;
        return ob_get_clean();
    }