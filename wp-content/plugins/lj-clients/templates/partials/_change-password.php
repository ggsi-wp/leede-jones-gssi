<?php
/**
 * Change password 
 */
namespace LJClient\Security;

function change_password()
{   
    require_once( SECURITYFORM . 'security.php' ); 

    ob_start();
        if( isset( $_POST['change-password'] ) )
        {   exit( print_r( $_POST ) );
            if( TRUE === ( $res = \Security\verify() ) )
            {   
                \LJClient\Security\writeResponses();
                \Security\loginLink();
            }
            else
            {   
                \LJClient\Security\form( $res );
            }
        }
        elseif( isset( $_REQUEST['step'] ) )
        {
            $res = \Security\verifyReset();
            if( 'WP_Error' == get_class( $res ) )   // Well, that didn't work
            {
                // Echo out the errors
                ?>
                <div class="errors">
                    <?php echo \LJClient\Security\writeErrors( $res->errors ); ?>
                </div>
                <?php
            }
            else
            {
                \LJClient\Security\form();
            }
        }
        else
        {   
            \LJClient\Security\form();
        }
    return ob_get_clean();
}

function form( $res = NULL )
{   
    $questions  = getQuestions(); 

    $qCount     = esc_attr( get_option( 'security_questions_required' ) ); 

/*    update_post_meta( $post_id, 'client_secqs', json_encode( $questions ) );
    update_post_meta( $post_id, 'client_seqas', json_encode( $answers ) );*/

    ob_start(); 
        //Present password change form
        echo \Security\changePassword( $res );
        

        if( is_user_logged_in() )
        {
            $cQuestions = json_decode( get_post_meta( $_SESSION['clientId'], 'client_secqs', TRUE ) ); 
            $cAnswers   = json_decode( get_post_meta( $_SESSION['clientId'], 'client_seqas', TRUE ) );
            
            if( $_SESSION['client_password_change'] )
            {
                if( $_SESSION['client_security_change'] )
                {
                    // Present security questions form
                    echo \Security\changeSecurity( $questions, $cQuestions, $cAnswers, $qCount );
                }
            }
            else
            {
                // Present security questions form
                echo \Security\changeSecurity( $questions, $cQuestions, $cAnswers, $qcount );
            }
        }
        else
        {
            $clientId   = getClientId( $res->user_id );
            $cQuestions = json_decode( get_post_meta( $clientId, 'client_secqs', TRUE ) ); 
            $cAnswers   = json_decode( get_post_meta( $clientId, 'client_seqas', TRUE ) );            
            
            // Present password change form and random security question
            echo \Security\askSecurity( $questions, $cQuestions );
        }

        echo \Security\endChange();

        $o  = ob_get_clean(); 
        echo $o;
}

/**
 * Write the responses to the form
 */
function writeResponses()
{
    if( is_user_logged_in() )
    {
        $userId     = get_current_user_id();
        $clientId   = $_SESSION['clientId'];
    }
    else
    {
        $userId     = filter_var( $_REQUEST['user_id'], FILTER_VALIDATE_INT );
        $clientId   = getClientId( $userId );
    }
    var_dump( $userId ); var_dump( $clientId );
    wp_update_user( [
        'ID'        => $userId,
        'user_pass' => filter_var( $_POST['new-password'], FILTER_SANITIZE_STRING )
    ] );


    update_post_meta( $clientId, 'client_password_change', FALSE );

    if( isset( $_POST['client_secq1'] ) )   // There are security questions too
    {
        $qCount     = esc_attr( get_option( 'security_questions_required' ) );

        for( $i=1; $i<=$qCount; $i++ )
        {
            $questions[]    = $_POST['client_secq' . $i];
            $answers[]      = sanitize_text_field( $_POST['client_secq' . $i . '_ans'] );
        }
    
        update_post_meta( $_SESSION['clientId'], 'client_secqs', json_encode( $questions ) );
        update_post_meta( $_SESSION['clientId'], 'client_seqas', json_encode( $answers ) );
    }
}

function writeErrors( $errors )
{
    $o  = [];
    foreach( $errors as $e )
    {
        $o[]    = join( "<br />", $e );
    }
    return join( "<br />", $o );
}