<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'admin_menu', 'leede_add_clients_page', 1 );
function leede_add_clients_page() {
	add_menu_page(
		'Clients',
		'Clients',
		'edit_posts',
		'clients',
		'leede_clients_output',
		'dashicons-admin-users'
	);
	add_submenu_page(
		'clients',
		'Registration Test',
		'Registration Test',
		'edit_posts',
		'registration_test',
		'leede_registration_test_output'
	);
	add_submenu_page(
		'clients',
		'Logs & Reporting',
		'Logs & Reporting',
		'edit_posts',
		'login_report',
		'leede_login_report_output'
	);
}

function leede_clients_output() {
	ob_start();

	if ( isset( $_GET['subpage'] ) && sanitize_text_field( $_GET['subpage'] ) == 'edit' ) {
		include_once 'views/view-page-clients-edit.php';
	} elseif ( isset( $_GET['subpage'] ) && sanitize_text_field( $_GET['subpage'] ) == 'delete' ) {
		include_once 'views/view-page-clients-delete.php';
	} // Display a list with all the clients
	else {
		include_once 'views/view-page-clients-list.php';
	}
	$clients_output = ob_get_contents();
	ob_clean();
	echo $clients_output;
}

function leede_registration_test_output() {
	ob_start();

	include_once 'views/view-page-registration-test.php';

	$login_report_output = ob_get_contents();
	ob_clean();
	echo $login_report_output;
}

function leede_login_report_output() {
	ob_start();

	include_once 'views/view-page-logs-reporting.php';

	$login_report_output = ob_get_contents();
	ob_clean();
	echo $login_report_output;
}

function leede_save_client( $id, $data ) {
	$id = (int) $id;
	if ( ! $id ) {
		return false;
	}
	$post_id      = '';
	$client_query = new WP_Query(
		array(
			'post_type'  => 'client',
			'meta_query' => array(
				array(
					'key'   => 'client_user_id',
					'value' => $id
				)
			)
		) );
	while ( $client_query->have_posts() ) :
		$client_query->the_post();
		$post_id = get_the_ID();
	endwhile;
	wp_reset_postdata();

	if ( isset( $data['new_password'] ) && $data['new_password'] != '' ) {
		wp_set_password( $data['new_password'], $id );
	}
	$first_name     = sanitize_text_field( wp_unslash( $data['first_name'] ) );
	$old_first_name = get_user_meta( $id, 'first_name', true );
	if ( ! empty( $data['first_name'] ) && $old_first_name != $data['first_name'] ) {
		update_user_meta( $id, 'first_name', $first_name );
		if ( ! empty( $post_id ) ) {
			update_post_meta( $post_id, 'client_firstname', $first_name );
		}
	}

	$last_name     = sanitize_text_field( wp_unslash( $data['last_name'] ) );
	$old_last_name = get_user_meta( $id, 'last_name', true );
	if ( ! empty( $data['last_name'] ) && $old_last_name != $data['last_name'] ) {
		update_user_meta( $id, 'last_name', $last_name );
		if ( ! empty( $post_id ) ) {
			update_post_meta( $post_id, 'client_lastname', $last_name );
		}
	}
	$phone_number1     = sanitize_text_field( wp_unslash( $data['phone_number1'] ) );
	$old_phone_number1 = get_user_meta( $id, 'phone_number1', true );
	if ( ! empty( $data['phone_number1'] ) && $old_phone_number1 != $data['phone_number1'] ) {
		update_user_meta( $id, 'phone_number1', $phone_number1 );
	}
	$phone_number2     = sanitize_text_field( wp_unslash( $data['phone_number2'] ) );
	$old_phone_number2 = get_user_meta( $id, 'phone_number2', true );
	if ( ! empty( $data['phone_number2'] ) && $old_phone_number2 != $data['phone_number2'] ) {
		update_user_meta( $id, 'phone_number2', $phone_number2 );
	}
	$phone_number3     = sanitize_text_field( wp_unslash( $data['phone_number3'] ) );
	$old_phone_number3 = get_user_meta( $id, 'phone_number3', true );
	if ( ! empty( $data['phone_number3'] ) && $old_phone_number3 != $data['phone_number3'] ) {
		update_user_meta( $id, 'phone_number3', $phone_number3 );
	}
	$address1     = sanitize_text_field( wp_unslash( $data['address1'] ) );
	$old_address1 = get_user_meta( $id, 'address1', true );
	if ( ! empty( $data['address1'] ) && $old_address1 != $data['address1'] ) {
		update_user_meta( $id, 'address1', $address1 );
	}
	$address2     = sanitize_text_field( wp_unslash( $data['address2'] ) );
	$old_address2 = get_user_meta( $id, 'address2', true );
	if ( ! empty( $data['address2'] ) && $old_address2 != $data['address2'] ) {
		update_user_meta( $id, 'address2', $address2 );
	}
	$old_country = get_user_meta( $id, 'country', true );
	if ( ! empty( $data['country'] ) && $old_country != $data['country'] ) {
		$country = sanitize_text_field( wp_unslash( $data['country'] ) );
		update_user_meta( $id, 'country', $country );
	}
	$old_province = get_user_meta( $id, 'province', true );
	if ( ! empty( $data['province'] ) && $old_province != $data['province'] ) {
		$province = sanitize_text_field( wp_unslash( $data['province'] ) );
		update_user_meta( $id, 'province', $province );
	}
	$city     = sanitize_text_field( wp_unslash( $data['city'] ) );
	$old_city = get_user_meta( $id, 'city', true );
	if ( ! empty( $data['city'] ) && $old_city != $data['city'] ) {
		update_user_meta( $id, 'city', $city );
	}
	$postal_code     = sanitize_text_field( wp_unslash( $data['postal_code'] ) );
	$old_postal_code = get_user_meta( $id, 'postal_code', true );
	if ( ! empty( $data['postal_code'] ) && $old_postal_code != $data['postal_code'] ) {
		update_user_meta( $id, 'postal_code', $postal_code );
	}
	$old_advisor = get_user_meta( $id, 'advisor', true );
	if ( ! empty( $data['advisor'] ) && $old_advisor != $data['advisor'] ) {
		$advisor = sanitize_text_field( wp_unslash( $data['advisor'] ) );
		update_user_meta( $id, 'advisor', $advisor );
		if ( ! empty( $post_id ) ) {
			update_post_meta( $post_id, 'client_advisor', $advisor );
		}
	}
	$old_client_status = get_user_meta( $id, 'client_status', true );
	if ( ! empty( $data['client_status'] ) && $old_client_status != $data['client_status'] ) {
		$client_status = sanitize_text_field( wp_unslash( $data['client_status'] ) );
		update_user_meta( $id, 'client_status', $client_status );
	}
	$old_client_code = get_user_meta( $id, 'client_code', true );
	if ( ! empty( $data['client_code'] ) && $old_client_code != $data['client_code'] ) {
		$client_code = sanitize_text_field( wp_unslash( $data['client_code'] ) );
		update_user_meta( $id, 'client_code', $client_code );
		if ( ! empty( $post_id ) ) {
			$dAccountTwoD = leede_get_account_number( $client_code );
			$client_doxim = ! empty( $dAccountTwoD ) ? 1 : 0;
			update_post_meta( $post_id, 'client_doxim', $client_doxim );
			update_post_meta( $post_id, 'client_doxim_accts', serialize( $dAccountTwoD ) );
		}
	}
	$old_client_code_broadridge = get_user_meta( $id, 'client_code_broadridge', true );
	if ( ! empty( $data['client_code_broadridge'] ) && $old_client_code_broadridge != $data['client_code_broadridge'] ) {
		$client_code_broadridge = sanitize_text_field( wp_unslash( $data['client_code_broadridge'] ) );
		update_user_meta( $id, 'client_code_broadridge', $client_code_broadridge );
		if ( ! empty( $post_id ) ) {
			update_post_meta( $post_id, 'client_broadridge_username', $client_code_broadridge );
		}
	}

	return true;
}

function leede_get_client_code_in_post( $id ) {
	$id                = (int) $id;
	$client_code_array = array();
	$client_code       = '';
	if ( ! $id ) {
		return '';
	}
	$post_id      = '';
	$client_query = new WP_Query(
		array(
			'post_type'  => 'client',
			'meta_query' => array(
				array(
					'key'   => 'client_user_id',
					'value' => $id
				)
			)
		) );
	while ( $client_query->have_posts() ) :
		$client_query->the_post();
		$post_id = get_the_ID();
	endwhile;
	wp_reset_postdata();
	if ( ! empty( $post_id ) ) {
		$dAccounts = unserialize( get_post_meta( $post_id, 'client_doxim_accts', true ) );
		if ( ! empty( $dAccounts ) ) {
			foreach ( $dAccounts as $dAccount ) {
				$client_code_array[] = $dAccount[1];
			}
			$client_code = join( ',', array_unique( $client_code_array ) );
		}
	}

	return $client_code;
}

function leede_user_id_exists( $user ) {
	global $wpdb;
	$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->users WHERE ID = %d", $user ) );
	if ( $count == 1 ) {
		return true;
	} else {
		return false;
	}
}

function leede_get_account_number( $client_code ) {
	$account_array = array();
	global $leede_options;
	if ( $leede_options['mssql_servername'] && $leede_options['mssql_database'] && $leede_options['mssql_uid'] && $leede_options['mssql_pwd'] ) {
		$mssql_servername = $leede_options['mssql_servername'];
		$mssql_database   = $leede_options['mssql_database'];
		$mssql_uid        = $leede_options['mssql_uid'];
		$mssql_pwd        = $leede_options['mssql_pwd'];
	} else {
		$mssql_servername = '67.231.22.62\\sqlexpress,1433';
		$mssql_database   = 'LeedeJones';
		$mssql_uid        = 'leedejones';
		$mssql_pwd        = 'aardvark88';
	}
	try {
		$serverName     = $mssql_servername;
		$connectionInfo = array( "Database" => $mssql_database, "UID" => $mssql_uid, "PWD" => $mssql_pwd );
		$conn           = sqlsrv_connect( $serverName, $connectionInfo );

		$sql  = "SELECT DISTINCT Client_Code, Account_Number FROM DailySyncData WHERE Client_Code IN (" . $client_code . ")";
		$stmt = sqlsrv_query( $conn, $sql );

		while ( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
			$account_array[] = array( $r['Account_Number'], $r['Client_Code'] );
		}
		sqlsrv_free_stmt( $stmt );
		sqlsrv_close( $conn );
	} catch ( Exception $e ) {

	}

	return $account_array;
}

function leede_get_client( $id ) {
	$client = get_userdata( $id );
	if ( ! $client || $client->roles[0] != 'client' ) {
		return false;
	} else {
		return $client;
	}
}

function leede_getAdvisors() {
	global $wpdb;
	$sql = "SELECT * FROM {$wpdb->prefix}connections ORDER BY first_name ,last_name";
	$res = $wpdb->get_results( $sql );
	$o   = [];
	foreach ( $res as $r ) {
		$o[ $r->id ] = str_replace( ',', '', $r->first_name . ' ' . $r->last_name );
	}

	return $o;
}

function leede_getAdvisor( $id ) {
	$id = (int) $id;
	if ( ! $id ) {
		return '';
	}
	global $wpdb;
	$sql = "SELECT last_name, first_name FROM {$wpdb->prefix}connections WHERE id='" . $id . "'";
	$res = $wpdb->get_results( $sql );

	return str_replace( ',', '', $res[0]->first_name . ' ' . $res[0]->last_name );
}

function leede_getIaCode( $id ) {
	$id = (int) $id;
	if ( ! $id ) {
		return '';
	}
	global $wpdb;
	$sql = "SELECT organization FROM {$wpdb->prefix}connections WHERE id='" . $id . "'";
	$res = $wpdb->get_results( $sql );

	return $res[0]->organization;
}

function leede_getAdvisorEmail( $id ) {
	$id = (int) $id;
	if ( ! $id ) {
		return false;
	}
	global $wpdb;
	$sql = "SELECT email FROM {$wpdb->prefix}connections WHERE id='" . $id . "'";
	$res = $wpdb->get_results( $sql );

	return unserialize( $res[0]->email )[0]['address'];
}

function leede_get_result( $get_result = array(), $check = '' ) {
	if ( ! empty( $get_result ) ) {
		$count     = 0;
		$positions = array();
		$i         = 0;
		foreach ( $get_result as $key => $value ) {
			if ( ! empty( $value['fail_position'] ) ) {
				if ( in_array( $check, $value['fail_position'], true ) ) {
					$count ++;
					$positions[] = $i;
				}
				$i ++;
			}
		}
		if ( $count != 0 && $count != count($get_result)) {
			foreach ( $positions as $position ) {
				unset( $get_result[ $position ] );
			}
		}
		$get_result = array_values( $get_result );
	}

	return $get_result;
}

add_action( 'wp_login', 'leede_login_success', 10, 2 );
function leede_login_success( $user_login, $user ) {
	if ( in_array( 'client', $user->roles ) ) {
		$id                    = $user->ID;
		$user_login            = $user->data->user_login;
		$login_report_status   = "success";
		$now                   = new DateTime();
		$login_report_datetime = $now->format( 'Y-m-d H:i:s' );
		$client_status         = get_user_meta( $id, 'client_status', true );

		$referrer = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '';
		if ( ! empty( $referrer && ! empty( $client_status ) && $client_status == 'pending' ) ) {
			wp_logout();
			$expiry = time() + 1;
			setcookie( 'login_failed', 'failed', $expiry );
			setcookie( 'username_failed', $user_login, $expiry );
			wp_redirect( $referrer );
			exit;
		}

		update_user_meta( $id, 'last_login', time() );
		global $wpdb;
		$wpdb->insert( $wpdb->prefix . 'login_report', array(
			'ID'                    => $id,
			'user_login'            => $user_login,
			'login_report_status'   => $login_report_status,
			'login_report_datetime' => $login_report_datetime
		) );

	}
}

function leede_get_last_login( $id ) {
	$last_login = get_user_meta( $id, 'last_login', true );
	if ( $last_login ) {
		$the_login_date = human_time_diff( $last_login );

		return $the_login_date;
	} else {
		return '';
	}
}

add_action( 'wp_login_failed', 'leede_login_failed' );
function leede_login_failed( $username ) {
	$client = '';
	if ( username_exists( $username ) ) {
		$client = get_user_by( 'login', $username );
	} elseif ( email_exists( $username ) ) {
		$client = get_user_by( 'email', $username );
	}
	if ( $client != '' ) {
		if ( in_array( 'client', $client->roles ) ) {
			$ID                    = $client->ID;
			$user_login            = $client->data->user_login;
			$login_report_status   = "failure";
			$now                   = new DateTime();
			$login_report_datetime = $now->format( 'Y-m-d H:i:s' );
			global $wpdb;
			$wpdb->insert( $wpdb->prefix . 'login_report', array(
				'ID'                    => $ID,
				'user_login'            => $user_login,
				'login_report_status'   => $login_report_status,
				'login_report_datetime' => $login_report_datetime
			) );
		}
	}
	$referrer = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '';
	if ( ! empty( $referrer ) && ! strstr( $referrer, 'wp-login' ) && ! strstr( $referrer, 'wp-admin' ) ) {
		$expiry = time() + 1;
		setcookie( 'login_failed', 'failed', $expiry );
		setcookie( 'username_failed', $username, $expiry );
		wp_redirect( $referrer );
		exit;
	}
}

function leede_getTplPageURL( $TEMPLATE_NAME ) {
	$url   = '#';
	$pages = get_pages( array(
		'meta_key'   => '_wp_page_template',
		'meta_value' => $TEMPLATE_NAME
	) );
	if ( isset( $pages[0] ) ) {
		$url = get_page_link( $pages[0]->ID );
	}

	return $url;
}

function leede_getCellphone( $cellphone ) {
	$trim_cellphone = trim( $cellphone );
	$result         = str_replace( ' ', '-', $trim_cellphone );

	return $result;
}

add_action( 'admin_enqueue_scripts', 'leede_processOnPageLoad' );
add_action( 'wp_enqueue_scripts', 'leede_processOnPageLoad' );
function leede_processOnPageLoad() {
	if ( is_user_logged_in() && in_array( 'client', wp_get_current_user()->roles ) ) {
		global $leede_options;
		$allowed_time = $leede_options['login_timeout_time'] ? $leede_options['login_timeout_time'] : 3600;
		if(isset($_SESSION['broadridge'])) { ?>
            <script>
                function idleLogout() {
                    var t;
                    var delay = <?php echo $allowed_time*1000 ?> + 10000;
                    window.onload = resetTimer;
                    window.onmousemove = resetTimer;
                    window.onmousedown = resetTimer;  // catches touchscreen presses as well
                    window.ontouchstart = resetTimer; // catches touchscreen swipes as well
                    window.onclick = resetTimer;      // catches touchpad clicks as well
                    window.onkeydown = resetTimer;
                    window.addEventListener('scroll', resetTimer, true); // improved; see comments

                    function resetTimer() {
                        clearTimeout(t);
                        t = setTimeout(function(){ window.location.href = "<?php echo 'https://clientlogin.leedejonesgable.com/cgi-bin/wspd13_cgi.sh/iapr/iapars_x.p?file=other/logout.htm'; ?>"; }, delay);
                    }
                }
                idleLogout();
            </script>
        <?php } else {?>
            <script>
                function idleLogout() {
                    var t;
                    var delay = <?php echo $allowed_time*1000 ?> + 10000;
                    window.onload = resetTimer;
                    window.onmousemove = resetTimer;
                    window.onmousedown = resetTimer;  // catches touchscreen presses as well
                    window.ontouchstart = resetTimer; // catches touchscreen swipes as well
                    window.onclick = resetTimer;      // catches touchpad clicks as well
                    window.onkeydown = resetTimer;
                    window.addEventListener('scroll', resetTimer, true); // improved; see comments

                    function resetTimer() {
                        clearTimeout(t);
                        t = setTimeout(function(){ window.location.href = "<?php echo wp_logout_url( leede_getTplPageURL( 'templates/page-login.php' ) ); ?>"; }, delay);
                    }
                }
                idleLogout();
            </script>
        <?php }
	}
}

add_action( 'admin_init', 'leede_export_login_report' );
function leede_export_login_report() {
	if ( isset( $_POST['export_login_report'] ) ) {
		header( 'Content-type: text/csv' );
		header( 'Content-Disposition: attachment; filename="export_login_report' . time() . '.csv"' );
		header( 'Pragma: no-cache' );
		header( 'Expires: 0' );

		$file = fopen( 'php://output', 'w' );

		fputcsv( $file, array(
			'Username',
			'Login Status',
			'Datetime',
		) );
		global $wpdb;
		$sql     = "SELECT * FROM {$wpdb->prefix}login_report WHERE login_report_datetime >= ( NOW() - INTERVAL 30 DAY )";
		$results = $wpdb->get_results( $sql, ARRAY_A );
		if ( ! empty( $results ) ) {
			foreach ( $results as $result ) {
				fputcsv( $file, array(
					$result['user_login'],
					$result['login_report_status'],
					$result['login_report_datetime']
				) );
			}
		}
		exit();
	}
}
