<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$search_string = '';
if ( ! empty( $_GET['name'] ) ) {
	$search_string = trim( $_GET['name'] );
}

$count_args = array(
	'role'           => 'client',
	'fields'         => 'all_with_meta',
	'number'         => 999999,
	'search'         => "*{$search_string}*",
	'search_columns' => array(
		'display_name'
	),
	'meta_query'     => array(
		'relation' => 'AND'
	)
);


$user_count_query = new WP_User_Query( $count_args );

$user_count = $user_count_query->get_results();

// count the number of users found in the query
$total_users = $user_count ? count( $user_count ) : 0;

// grab the current page number and set to 1 if no page number is set
$page = ! empty( $_GET['paged'] ) ? sanitize_text_field( $_GET['paged'] ) : 1;

// how many users to show per page
$users_per_page = 10;

// calculate the total number of pages.
$total_pages = 1;
$offset      = $users_per_page * ( $page - 1 );
$total_pages = ceil( $total_users / $users_per_page );

// main user query
$args = array(
	'role'           => 'client',
	'orderby'        => 'ID',
	'order'          => 'DESC',
	// return all fields
	'fields'         => 'all_with_meta',
	'number'         => $users_per_page,
	'offset'         => $offset,
	'search'         => "*{$search_string}*",
	'search_columns' => array(
		'display_name'
	),
	'meta_query'     => array(
		'relation' => 'AND'
	)
);


// Create the WP_User_Query object
$wp_user_query = new WP_User_Query( $args );
// Get the results
$clients = $wp_user_query->get_results();
?>
<div class="wrap">
    <h1 class="wp-heading-inline"><?php echo esc_html__( 'Clients', 'leede' ) ?></h1>
    <hr class="wp-header-end">
    <form id="clients-filter" method="get">
        <p>
            <label class="screen-reader-text" for="name">Search Clients:</label>
            <input type="hidden" name="page" value="clients"/>
            <input type="text" name="name" id="name" value="<?php echo isset( $_GET['name'] ) ? esc_attr( $_GET['name'] ) : ''; ?>"/>
            <input type="submit" name="filter_action" class="button" value="Search Clients">
        </p>
	    <?php
	    if ( count( $clients ) > 0 ) { ?>
            <table class="wp-list-table widefat fixed striped table-view-list posts">
                <thead>
                <tr>
                    <th><?php echo esc_html__( 'Username', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'First Name', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'Last Name', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'Advisor', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'Account Locked', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'Status', 'leede' ) ?></th>
                    <th><?php echo esc_html__( 'Last Logged In', 'leede' ) ?></th>
                </tr>
                </thead>
                <tbody>
			    <?php foreach ( $clients as $client ) { ?>
                    <tr>
                        <td>
                            <strong>
                                <a href="<?php echo '?page=clients&subpage=edit&client_id='.$client->ID.''; ?>">
                                    <?php echo $client->data->user_login ?>
                                </a>
                            </strong>
                            <br>
                            <div class="row-actions">
                                <span class="edit">
                                    <a href="<?php echo '?page=clients&subpage=edit&client_id='.$client->ID.''; ?>">
                                        <?php echo esc_html__( 'Edit', 'leede' ); ?>
                                    </a> |
                                </span>
                                <span class="delete">
                                    <a href="<?php echo '?page=clients&subpage=delete&client_id='.$client->ID.''; ?>">
                                        <?php echo esc_html__( 'Delete', 'leede' ); ?>
                                    </a>
                                </span>
                            </div>
                        </td>
                        <td><?php echo get_user_meta( $client->ID, 'first_name', true ); ?></td>
                        <td><?php echo get_user_meta( $client->ID, 'last_name', true ); ?></td>
                        <td><?php echo leede_getAdvisor(get_user_meta( $client->ID, 'advisor', true )); ?></td>
                        <td></td>
                        <td style="text-transform: capitalize"><?php echo get_user_meta( $client->ID, 'client_status', true ); ?></td>
                        <td><?php echo leede_get_last_login($client->ID) ?></td>
                    </tr>
			    <?php } ?>
                </tbody>
            </table>
            <!-- Pager -->
            <div class="tablenav bottom">
                <div class="tablenav-pages">
                    <span class="displaying-num">
                        <?php echo $total_users ?>
                        <?php
                        if ( $total_users > 1 ) {
	                        echo esc_html__( 'items', 'leede' );
                        } else {
	                        echo esc_html__( 'item', 'leede' );
                        }
                        ?>
                    </span>
                    <span class="pagination-links">
                        <?php if ( $page > 1 ) { ?>
                            <a href="<?php echo remove_query_arg( array( 'paged' ) ) ?>"
                               class="tablenav-pages-navspan button" aria-hidden="true">«</a>
                            <a href="<?php echo add_query_arg( array( 'paged' => $page - 1 ) ) ?>"
                               class="tablenav-pages-navspan button" aria-hidden="true">‹</a>
                        <?php } else { ?>
                            <span class="tablenav-pages-navspan button disabled" aria-hidden="true">«</span>
                            <span class="tablenav-pages-navspan button disabled" aria-hidden="true">‹</span>
                        <?php } ?>
                        <span class="screen-reader-text"><?php echo esc_html__( 'Current Page', 'leede' ); ?></span>
                        <span id="table-paging" class="paging-input">
                            <span class="tablenav-paging-text"><?php echo $page; ?>
	                            <?php echo esc_html__( 'of', 'leede' ); ?>
                                <span class="total-pages"><?php echo $total_pages; ?></span>
                            </span>
                        </span>
					    <?php if ( $page < $total_pages ) { ?>
                            <a href="<?php echo add_query_arg( array( 'paged' => $page + 1 ) ) ?>"
                               class="next-page button">
                                <span class="screen-reader-text"><?php echo esc_html__( 'Next page', 'leede' ); ?></span>
                                <span aria-hidden="true">›</span>
                            </a>
                            <a href="<?php echo add_query_arg( array( 'paged' => $total_pages ) ) ?>"
                               class="last-page button">
                                <span class="screen-reader-text"><?php echo esc_html__( 'Last page', 'leede' ); ?></span>
                                <span aria-hidden="true">»</span>
                            </a>
					    <?php } else { ?>
                            <span class="next-page button disabled">
                                <span class="screen-reader-text"><?php echo esc_html__( 'Next page', 'leede' ); ?></span>
                                <span aria-hidden="true">›</span>
                            </span>
                            <span class="last-page button disabled">
                                <span class="screen-reader-text"><?php echo esc_html__( 'Last page', 'leede' ); ?></span>
                                <span aria-hidden="true">»</span>
                            </span>
					    <?php } ?>
                    </span>
                </div>
            </div>
		    <?php
	    } else {
		    echo esc_html__( 'No found', 'leede' );
	    }
	    ?>
    </form>

	<?php wp_reset_query(); ?>
</div>