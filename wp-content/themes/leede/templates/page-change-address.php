<?php
/**
 * Template Name: Leede Change Address
 *
 */
if ( ! is_user_logged_in() || ( is_user_logged_in() && ! in_array( 'client', wp_get_current_user()->roles ) ) ) {
	wp_redirect( home_url() );
	exit;
}
global $leede_options;
get_header();
?>
    <div class="container yyet leede-form leede-manage-profile">
        <div class="container_inner default_template_holder clearfix page_container_inner">
            <div class="leede-container">
                <div class="leede-col-3">
	                <?php echo do_shortcode('[lj-client-dashboard]');?>
                </div>
                <div class="leede-col-9">
                    <h2><?php echo esc_html__( 'Change Of Address Request Form', 'leede' ); ?></h2>
	                <?php
	                //Change Address
	                if ( $leede_options['manage_profile_address_content'] ) {
		                echo do_shortcode( $leede_options['manage_profile_address_content'] );
	                }
	                ?>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();