<?php
/**
 * Template Name: Leede Register
 *
 */
if ( is_user_logged_in() ) {
	wp_redirect( home_url() );
	exit;
}
global $leede_options;
if ( ! isset( $_SESSION ) ) {
	session_start();
}
if ( isset( $_POST['restart_form'] ) ) {
	session_unset();
}
if ( ! empty( $_GET['send_security_code'] ) ) {
	$_SESSION['register_step']['send_security_code'] = $_GET['send_security_code'];
	$send_security_code_array                        = explode( ' ', base64_decode( $_SESSION['register_step']['send_security_code'] ) );
	$_SESSION['register_step']['username']           = $send_security_code_array[0];
	$_SESSION['register_step']['time']               = isset( $send_security_code_array[1] ) ? $send_security_code_array[1] : 0;
	$register_step1_expired_time                     = ! empty( $leede_options['register_step1_expired_time'] ) ? $leede_options['register_step1_expired_time'] : 18000;
	if ( ! username_exists( $_SESSION['register_step']['username'] ) && ! email_exists( $_SESSION['register_step']['username'] ) && time() - $_SESSION['register_step']['time'] <= $register_step1_expired_time ) {
		$_SESSION['register_step']['send_security_code'] = 'valid';
	} else {
		$_SESSION['register_step']['send_security_code'] = 'expired';
	}
	wp_redirect( remove_query_arg( 'send_security_code' ) );
	exit;
}
require __DIR__ . '/../twilio-php-master/src/Twilio/autoload.php';
//Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

// Your Account SID and Auth Token from twilio.com/console

//API Credentials - Twilio
if ( $leede_options['twilio_phone_number'] && $leede_options['twilio_sid'] && $leede_options['twilio_token'] ) {
	$twilio_phone_number = $leede_options['twilio_phone_number'];
	$twilio_sid          = $leede_options['twilio_sid'];
	$twilio_token        = $leede_options['twilio_token'];
} else {
	$twilio_phone_number = '8167223210';//'+15005550006';
	$twilio_sid          = 'AC1b9ac518508cb3fbe05dc69412801763';//'ACb0a4bb3002502712ea208171727794cb';
	$twilio_token        = '1cfbbb3906c41e28a3bdef0412bbecf7';//'c69eb7a273c057f938ebb4ccf8071b43';
}

if ( $leede_options['mssql_servername'] && $leede_options['mssql_database'] && $leede_options['mssql_uid'] && $leede_options['mssql_pwd'] ) {
	$mssql_servername = $leede_options['mssql_servername'];
	$mssql_database   = $leede_options['mssql_database'];
	$mssql_uid        = $leede_options['mssql_uid'];
	$mssql_pwd        = $leede_options['mssql_pwd'];
} else {
	$mssql_servername = '67.231.22.62\\sqlexpress,1433';
	$mssql_database   = 'LeedeJones';
	$mssql_uid        = 'leedejones';
	$mssql_pwd        = 'aardvark88';
}
get_header();
?>
    <div class="container yyet leede-form">
        <div class="container_inner default_template_holder clearfix page_container_inner">
			<?php if ( isset( $_POST['submit_register_step2'] ) ) { ?>
				<?php
				$register_status = 'failed';
				$get_result      = array();

				$register_step2_failed_count   = ! empty( $leede_options['register_step2_failed_count'] ) ? (int) $leede_options['register_step2_failed_count'] : 3;
				$register_step2_failed_matches = ! empty( $leede_options['register_step2_failed_matches'] ) ? (int) $leede_options['register_step2_failed_matches'] : 2;

				$password      = $_POST['password'];
				$client_status = 'pending';

				$home_phone             = '';
				$account_number         = '';
				$client_code            = '';
				$client_code_broadridge = '';

				$last_name           = trim( $_POST['last_name'] ); //Last_Name_Client
				$compare_last_name   = preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $last_name ) );
				$country             = $_POST['country']; //Country
				$compare_country     = $country != 'CA' ? $country : 'CAN';
				$province            = $_POST['province']; //Province_State
				$compare_province    = $province;
				$postal_code         = trim( $_POST['postal_code'] ); //Postal_Code_ZIP
				$compare_postal_code = preg_replace( '/[^A-Za-z0-9]/', '', $postal_code );

				$first_name           = trim( $_POST['first_name'] ); //First_Name_Client
				$compare_first_name   = preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $first_name ) );
				$phone_number1        = $_SESSION['register_step']['phone_number1']; //Cell_Phone
				$phone_number2        = $_SESSION['register_step']['phone_number2']; //Cell_Phone
				$phone_number3        = $_SESSION['register_step']['phone_number3']; //Cell_Phone
				$phone_number         = $phone_number1 . ' ' . $phone_number2 . ' ' . $phone_number3; //Cell_Phone
				$compare_phone_number = substr( preg_replace( '/[^A-Za-z0-9]/', '', $phone_number ), - 7 );
				$advisor              = $_POST['advisor'];
				$advisor_email        = leede_getAdvisorEmail( $advisor );
				$ia_client            = leede_getIaCode( $advisor ); //$_POST['']; //IA_Client
				$compare_ia_client    = strtolower( $ia_client );
				$address1             = trim( $_POST['address1'] );
				$address2             = trim( $_POST['address2'] );
				$address              = $address1 . ' ' . $address2; //AdPL_Line_2
				$compare_address      = preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $address ) );
				$city                 = trim( $_POST['city'] ); //City
				$compare_city         = preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $city ) );
				$email                = $_SESSION['register_step']['username']; //Email
				$compare_email        = strtolower( $email );

				try {
					$serverName     = $mssql_servername;
					$connectionInfo = array( "Database" => $mssql_database, "UID" => $mssql_uid, "PWD" => $mssql_pwd );
					$conn           = sqlsrv_connect( $serverName, $connectionInfo );

					$sql  = "SELECT * FROM DailySyncData WHERE REPLACE(Last_Name_Client, ' ','') = '" . $compare_last_name . "' AND Country = '" . $compare_country . "' AND Province_State = '" . $compare_province . "' AND REPLACE(REPLACE(Postal_Code_ZIP, ' ',''), '-', '') = '" . $compare_postal_code . "'";
					$stmt = sqlsrv_query( $conn, $sql );

					while ( $result = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC ) ) {
						$fail_counter        = 0;
						$fail_position       = array();
						$result_first_name   = ! is_null( $result['First_Name_Client'] ) ? preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $result['First_Name_Client'] ) ) : '';
						$result_phone_number = ! is_null( $result['Cell_Phone'] ) ? substr( preg_replace( '/[^A-Za-z0-9]/', '', $result['Cell_Phone'] ), - 7 ) : '';
						$result_ia_client    = ! empty( $result['IA_Client'] ) ? strtolower( $result['IA_Client'] ) : '';
						$result_AdPL_Line_2  = ! empty( $result['AdPL_Line_2'] ) ? strtolower( $result['AdPL_Line_2'] ) : '';
						$result_address      = preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $result_AdPL_Line_2 ) );
						$result_city         = ! empty( $result['City'] ) ? preg_replace( '/[^A-Za-z0-9]/', '', strtolower( $result['City'] ) ) : '';

						if ( strlen( $compare_city ) > strlen( $result_city ) ) {
							$compare_city = substr( $compare_city, 0, strlen( $result_city ) );
						} else {
							$result_city = substr( $result_city, 0, strlen( $compare_city ) );
						}
						$result_email = ! empty( $result['Email'] ) ? strtolower( $result['Email'] ) : '';

						if ( $compare_first_name != $result_first_name ) {
							$fail_counter    += 2;
							$fail_position[] = 'first_name';
						}
						if ( $compare_phone_number != $result_phone_number ) {
							$fail_counter    += 1;
							$fail_position[] = 'phone_number';
						}
						if ( $compare_ia_client != $result_ia_client ) {
							$fail_counter    += 1;
							$fail_position[] = 'ia_client';
						}
						if ( $compare_address != $result_address ) {
							$fail_counter    += 1;
							$fail_position[] = 'address';
						}
						if ( $compare_city != $result_city ) {
							$fail_counter    += 1;
							$fail_position[] = 'city';
						}
						if ( $compare_email != $result_email ) {
							$fail_counter    += 1;
							$fail_position[] = 'email';
						}
						$result_home_phone             = ! is_null( $result['Home_Phone'] ) ? $result['Home_Phone'] : '';
						$result_account_number         = ! is_null( $result['Account_Number'] ) ? $result['Account_Number'] : '';
						$result_client_code            = ! is_null( $result['Client_Code'] ) ? $result['Client_Code'] : '';
						$result_client_code_broadridge = ! is_null( $result['Comments_Docs'] ) ? $result['Comments_Docs'] : '';

						if ( $fail_counter <= $register_step2_failed_count ) {
							$get_result[] = array(
								'fail_counter'                  => $fail_counter,
								'fail_position'                 => $fail_position,
								'result_home_phone'             => $result_home_phone,
								'result_account_number'         => $result_account_number,
								'result_client_code'            => $result_client_code,
								'result_client_code_broadridge' => $result_client_code_broadridge
							);
						}
					}
					sqlsrv_free_stmt( $stmt );
					sqlsrv_close( $conn );

				} catch ( Exception $e ) {

				}

				$get_result = leede_get_result( $get_result, 'first_name' );
				$get_result = leede_get_result( $get_result, 'phone_number' );
				$get_result = leede_get_result( $get_result, 'ia_client' );
				$get_result = leede_get_result( $get_result, 'address' );
				$get_result = leede_get_result( $get_result, 'city' );
				$get_result = leede_get_result( $get_result, 'email' );

				if ( count( $get_result ) == 1 ) {
					$register_status        = 'success';
					$client_status          = 'active';
					$home_phone             = $get_result[0]['result_home'];
					$account_number         = $get_result[0]['result_account_number'];
					$client_code            = $get_result[0]['result_client_code'];
					$client_code_broadridge = $get_result[0]['result_client_code_broadridge'];
				}

				$userdata = array(
					'ID'           => '',
					'user_pass'    => $password,
					'user_login'   => $email,
					'user_email'   => $email,
					'display_name' => $first_name . ' ' . $last_name,
					'first_name'   => $first_name,
					'last_name'    => $last_name,
					'role'         => 'client',
				);
				$user_id  = wp_insert_user( $userdata );
				if ( ! is_wp_error( $user_id ) ) {
					add_user_meta( $user_id, 'client_status', $client_status, true );
					add_user_meta( $user_id, 'phone_number1', $phone_number1, true );
					add_user_meta( $user_id, 'phone_number2', $phone_number2, true );
					add_user_meta( $user_id, 'phone_number3', $phone_number3, true );
					add_user_meta( $user_id, 'address1', $address1, true );
					add_user_meta( $user_id, 'address2', $address2, true );
					add_user_meta( $user_id, 'country', $country, true );
					add_user_meta( $user_id, 'province', $province, true );
					add_user_meta( $user_id, 'city', $city, true );
					add_user_meta( $user_id, 'postal_code', $postal_code, true );
					add_user_meta( $user_id, 'advisor', $advisor, true );
					add_user_meta( $user_id, 'ia_client', $ia_client, true );

					add_user_meta( $user_id, 'home_phone', $home_phone, true );
					add_user_meta( $user_id, 'account_number', $account_number, true );
					add_user_meta( $user_id, 'client_code', $client_code, true );
					add_user_meta( $user_id, 'client_code_broadridge', $client_code_broadridge, true );
				}

				$client_broadridge_username = $client_code_broadridge;
				$passphrase                 = ! empty( $leede_options['passphrase'] ) ? $leede_options['passphrase'] : '';
				$client_broadridge_password = $client_code . $passphrase . $ia_client;
				$client_broadridge          = ! empty( $client_broadridge_username ) && ! empty( $client_broadridge_password ) ? 1 : 0;
				$dAccountTwoD               = leede_get_account_number( $client_code );
				$client_doxim               = ! empty( $dAccountTwoD ) ? 1 : 0;

				$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type'   => 'client'
				) );
				update_post_meta( $post_id, 'client_email', $email );
				update_post_meta( $post_id, 'client_firstname', $first_name );
				update_post_meta( $post_id, 'client_lastname', $last_name );
				update_post_meta( $post_id, 'client_advisor', $advisor );
				update_post_meta( $post_id, 'client_broadridge', $client_broadridge );
				update_post_meta( $post_id, 'client_broadridge_username', $client_broadridge_username );
				update_post_meta( $post_id, 'client_broadridge_password', $client_broadridge_password );
				update_post_meta( $post_id, 'client_doxim', $client_doxim );
				update_post_meta( $post_id, 'client_doxim_accts', serialize( $dAccountTwoD ) );
				update_post_meta( $post_id, 'client_user_id', $user_id );

				$logo_img = get_stylesheet_directory_uri() . '/images/logo.jpg';
				$to       = array( $advisor_email );
				if ( $leede_options['admin_email_address'] ) {
					$admin_email_address = explode( ";", $leede_options['admin_email_address'] );
					foreach ( $admin_email_address as $admin_email ) {
						$to[] = trim( $admin_email );
					}
				}

				if ( $register_status == 'success' ) {
					//Success - Email To Advisor
					if ( $leede_options['register_step2_success_email_subject'] ) {
						$subject = $leede_options['register_step2_success_email_subject'];
					} else {
						$subject = 'Successful Registration Information';
					}
					if ( $leede_options['register_step2_success_email_body'] ) {
						$body = do_shortcode( $leede_options['register_step2_success_email_body'] );
					} else {
						$body = '
                    <p><img alt="img" src="' . $logo_img . '"/></p>
                    <p>Dear</p>
                    <p>Your Client, <strong>' . $first_name . ' ' . $last_name . '</strong> has registered for online access to their files.  Their account has been validated and verified, they now have immediate access.</p>
                    <p>The following is their registration information provided.</p>
                    ';
					}
					$body .= '
                    <br>
                    <h2 style="color: #aaa;">Client Information</h2>
                    <table style="text-align: left; min-width: 70%">
                    <tr>
                    <th>First Name</th>
                    <td>' . $first_name . '</td>
                    </tr>
                    <tr>
                    <th>Last Name</th>
                    <td>' . $last_name . '</td>
                    </tr>
                    <tr>
                    <th>Country</th>
                    <td>' . $country . '</td>
                    </tr>
                    <tr>
                    <th>Address</th>
                    <td>' . $address . '</td>
                    </tr>
                    <tr>
                    <th>City</th>
                    <td>' . $city . '</td>
                    </tr>
                    <tr>
                    <th>Region (Province/State)</th>
                    <td>' . $province . '</td>
                    </tr>
                    <tr>
                    <th>Other Region</th>
                    <td></td>
                    </tr>
                    <tr>
                    <th>Postal/Zip Code</th>
                    <td>' . $postal_code . '</td>
                    </tr>
                    <tr>
                    <th>Telephone</th>
                    <td>' . leede_getCellphone( $home_phone ) . '</td>
                    </tr>
                    <tr>
                    <th>Cell phone</th>
                    <td>' . leede_getCellphone( $phone_number ) . '</td>
                    </tr>
                    <tr>
                    <th>Email</th>
                    <td>' . $email . '</td>
                    </tr>
                    <tr>
                    <th>Investment Advisor</th>
                    <td>' . leede_getAdvisor( $advisor ) . '</td>
                    </tr>
                    <tr>
                    <th>Account #</th>
                    <td>' . $account_number . '</td>
                    </tr>
                    <tr>
                    <th>User ID</th>
                    <td>' . $client_code . '</td>
                    </tr>
                    </table>
                    ';
					if ( $leede_options['register_step2_success_email_body'] ) {
						$body .= '<br>' . do_shortcode( $leede_options['register_step2_success_email_signature'] );
					} else {
						$body .= '<br>
                    <p><small>This is an automatically delivered message and replies to this address cannot be answered.</small></p>
                    <h3 style="font-weight: normal; padding-top: 15px; border-top: 1px solid #eee">© Leede Jones Gable Inc. 2020</h3>';
					}
					$headers = array( 'Content-Type: text/html; charset=UTF-8' );
					if ( $leede_options['register_step2_success_email_headers'] ) {
						$headers[] = $leede_options['register_step2_success_email_headers'];
					}
					wp_mail( $to, $subject, $body, $headers );
					?>
                    <h1><?php echo esc_html__( 'Thank You', 'leede' ); ?></h1>
					<?php
					//Registration Success
					if ( $leede_options['register_step2_success_content'] ) {
						echo do_shortcode( $leede_options['register_step2_success_content'] );
					}
					?>
				<?php } else {
					//Failed Email Notification - Incorrect Registration Information
					if ( $leede_options['register_step2_failed_email_subject'] ) {
						$subject = $leede_options['register_step2_failed_email_subject'];
					} else {
						$subject = 'Incorrect Registration Information';
					}
					if ( $leede_options['register_step2_failed_email_body'] ) {
						$body = do_shortcode( $leede_options['register_step2_failed_email_body'] );
					} else {
						$body = '
                    <p><img alt="img" src="' . $logo_img . '"/></p>
                    <p>Dear</p>
                    <p>A user has attempted to register for online access to their account. You were marked as their investment advisor.  The system was unable to verify their information fully, and as such they have NOT been provided immediate online approval.</p>
                    <p>The information they have submitted for online access is marked below, please finish validation of this online registration, have the online profile activated and then reach out to your client to inform them of account activation.</p>
                    ';
					}
					$body .= '
                    <br>
                    <h2 style="color: #aaa;">Client Information</h2>
                    <table style="text-align: left; min-width: 70%">
                    <tr>
                    <th>First Name</th>
                    <td>' . $first_name . '</td>
                    </tr>
                    <tr>
                    <th>Last Name</th>
                    <td>' . $last_name . '</td>
                    </tr>
                    <tr>
                    <th>Country</th>
                    <td>' . $country . '</td>
                    </tr>
                    <tr>
                    <th>Address</th>
                    <td>' . $address . '</td>
                    </tr>
                    <tr>
                    <th>City</th>
                    <td>' . $city . '</td>
                    </tr>
                    <tr>
                    <th>Region (Province/State)</th>
                    <td>' . $province . '</td>
                    </tr>
                    <tr>
                    <th>Other Region</th>
                    <td></td>
                    </tr>
                    <tr>
                    <th>Postal/Zip Code</th>
                    <td>' . $postal_code . '</td>
                    </tr>
                    <tr>
                    <th>Telephone</th>
                    <td>' . leede_getCellphone( $home_phone ) . '</td>
                    </tr>
                    <tr>
                    <th>Cell phone</th>
                    <td>' . leede_getCellphone( $phone_number ) . '</td>
                    </tr>
                    <tr>
                    <th>Email</th>
                    <td>' . $email . '</td>
                    </tr>
                    <tr>
                    <th>Investment Advisor</th>
                    <td>' . leede_getAdvisor( $advisor ) . '</td>
                    </tr>
                    </table>
                    ';
					if ( $leede_options['register_step2_failed_email_body'] ) {
						$body .= '<br>' . do_shortcode( $leede_options['register_step2_failed_email_signature'] );
					} else {
						$body .= '
					<br>
                    <p><small>This is an automatically delivered message and replies to this address cannot be answered.</small></p>
                    <h3 style="font-weight: normal; padding-top: 15px; border-top: 1px solid #eee">© Leede Jones Gable Inc. 2020</h3>
					';
					}
					$headers = array( 'Content-Type: text/html; charset=UTF-8' );
					if ( $leede_options['register_step2_failed_email_headers'] ) {
						$headers[] = $leede_options['register_step2_failed_email_headers'];
					}
					wp_mail( $to, $subject, $body, $headers );
					?>
                    <h1><?php echo esc_html__( 'Thank You', 'leede' ); ?></h1>
					<?php
					//Failed for Submitting a Registration
					if ( $leede_options['register_step2_failed_content'] ) {
						echo do_shortcode( $leede_options['register_step2_failed_content'] );
					}
					?>
				<?php }
				session_unset(); ?>
			<?php } else {
				if ( isset( $_SESSION['register_step']['username'] ) && isset( $_SESSION['register_step']['send_security_code'] ) && $_SESSION['register_step']['send_security_code'] == 'valid' ) { ?>
                    <h1><?php echo esc_html__( '3. Name and Address', 'leede' ); ?></h1>
					<?php
					//Registration - Step 2 - Verify Account
					if ( $leede_options['register_step2_verify_account_content'] ) {
						echo do_shortcode( $leede_options['register_step2_verify_account_content'] );
					}
					?>
                    <form method="post" id="cellphone-form" action="">
                        <input type="hidden" name="hidden_first_name"
                               value="<?php echo isset( $_POST['hidden_first_name'] ) ? $_POST['hidden_first_name'] : ''; ?>">
                        <input type="hidden" name="hidden_last_name"
                               value="<?php echo isset( $_POST['hidden_last_name'] ) ? $_POST['hidden_last_name'] : ''; ?>">
                        <input type="hidden" name="hidden_address1"
                               value="<?php echo isset( $_POST['hidden_address1'] ) ? $_POST['hidden_address1'] : ''; ?>">
                        <input type="hidden" name="hidden_address2"
                               value="<?php echo isset( $_POST['hidden_address2'] ) ? $_POST['hidden_address2'] : ''; ?>">
                        <input type="hidden" name="hidden_country"
                               value="<?php echo isset( $_POST['hidden_country'] ) ? $_POST['hidden_country'] : ''; ?>">
                        <input type="hidden" name="hidden_province"
                               value="<?php echo isset( $_POST['hidden_province'] ) ? $_POST['hidden_province'] : ''; ?>">
                        <input type="hidden" name="hidden_city"
                               value="<?php echo isset( $_POST['hidden_city'] ) ? $_POST['hidden_city'] : ''; ?>">
                        <input type="hidden" name="hidden_postal_code"
                               value="<?php echo isset( $_POST['hidden_postal_code'] ) ? $_POST['hidden_postal_code'] : ''; ?>">
                        <input type="hidden" name="hidden_advisor"
                               value="<?php echo isset( $_POST['hidden_advisor'] ) ? $_POST['hidden_advisor'] : ''; ?>">
                        <input type="hidden" name="hidden_password"
                               value="<?php echo isset( $_POST['hidden_password'] ) ? $_POST['hidden_password'] : ''; ?>">
                        <input type="hidden" name="hidden_confirm_password"
                               value="<?php echo isset( $_POST['hidden_confirm_password'] ) ? $_POST['hidden_confirm_password'] : ''; ?>">
                        <input type="hidden" name="phone_number1">
                        <input type="hidden" name="phone_number2">
                        <input type="hidden" name="phone_number3">
                        <input type="hidden" name="submit_sms_twilio">
                    </form>
                    <form method="post" id="code-form" action="">
                        <input type="hidden" name="hidden_first_name"
                               value="<?php echo isset( $_POST['hidden_first_name'] ) ? $_POST['hidden_first_name'] : ''; ?>">
                        <input type="hidden" name="hidden_last_name"
                               value="<?php echo isset( $_POST['hidden_last_name'] ) ? $_POST['hidden_last_name'] : ''; ?>">
                        <input type="hidden" name="hidden_address1"
                               value="<?php echo isset( $_POST['hidden_address1'] ) ? $_POST['hidden_address1'] : ''; ?>">
                        <input type="hidden" name="hidden_address2"
                               value="<?php echo isset( $_POST['hidden_address2'] ) ? $_POST['hidden_address2'] : ''; ?>">
                        <input type="hidden" name="hidden_country"
                               value="<?php echo isset( $_POST['hidden_country'] ) ? $_POST['hidden_country'] : ''; ?>">
                        <input type="hidden" name="hidden_province"
                               value="<?php echo isset( $_POST['hidden_province'] ) ? $_POST['hidden_province'] : ''; ?>">
                        <input type="hidden" name="hidden_city"
                               value="<?php echo isset( $_POST['hidden_city'] ) ? $_POST['hidden_city'] : ''; ?>">
                        <input type="hidden" name="hidden_postal_code"
                               value="<?php echo isset( $_POST['hidden_postal_code'] ) ? $_POST['hidden_postal_code'] : ''; ?>">
                        <input type="hidden" name="hidden_advisor"
                               value="<?php echo isset( $_POST['hidden_advisor'] ) ? $_POST['hidden_advisor'] : ''; ?>">
                        <input type="hidden" name="hidden_password"
                               value="<?php echo isset( $_POST['hidden_password'] ) ? $_POST['hidden_password'] : ''; ?>">
                        <input type="hidden" name="hidden_confirm_password"
                               value="<?php echo isset( $_POST['hidden_confirm_password'] ) ? $_POST['hidden_confirm_password'] : ''; ?>">
                        <input type="hidden" name="code_twilio">
                        <input type="hidden" name="submit_code_twilio">
                    </form>
                    <form method="post" class="register_form register_step2" action="">
                        <h3><?php echo esc_html__( 'Contact Information', 'leede' ); ?></h3>
                        <fieldset>
                            <label class="label-pl"><?php echo esc_html__( 'Email:', 'leede' ); ?></label>
                            <div class="field"><?php echo $_SESSION['register_step']['username']; ?></div>
                        </fieldset>
                        <fieldset>
                            <label for="first_name">
                                <span class="required">* </span><?php echo esc_html__( 'First Name:', 'leede' ); ?>
                            </label>
                            <div class="field">
                                <input id="first_name" type="text" name="first_name"
                                       value="<?php echo isset( $_POST['hidden_first_name'] ) ? $_POST['hidden_first_name'] : ''; ?>">
                                <span class="error error_first_name"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="last_name">
                                <span class="required">* </span><?php echo esc_html__( 'Last Name:', 'leede' ); ?>
                            </label>
                            <div class="field">
                                <input id="last_name" type="text" name="last_name"
                                       value="<?php echo isset( $_POST['hidden_last_name'] ) ? $_POST['hidden_last_name'] : ''; ?>">
                                <span class="error error_last_name"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label>
                                <span class="required">* </span><?php echo esc_html__( 'Cell Phone:', 'leede' ); ?>
                            </label>
                            <div class="field">
                                <div class="box box-phone" id="verify_phone_number">
									<?php
									if ( isset( $_POST['submit_sms_twilio'] ) && ( ! isset( $_SESSION['register_step']['verify_phone_number'] ) || ( isset( $_SESSION['register_step']['verify_phone_number'] ) && $_SESSION['register_step']['verify_phone_number'] != 'unsucessfully' ) ) ) {
										if ( isset( $_SESSION['register_step']['count_sms'] ) && $_SESSION['register_step']['count_sms'] >= 3 ) {
											$_SESSION['register_step']['count_sms'] = $_SESSION['register_step']['count_sms'] + 1;
										} else {
											$_SESSION['register_step']['count_code'] = isset( $_SESSION['register_step']['count_code'] ) ? $_SESSION['register_step']['count_code'] : 0;
											try {
												$client                                          = new Client( $twilio_sid, $twilio_token );
												$code_twilio                                     = rand( 1000000, 9999999 );
												$_SESSION['register_step']['code_twilio']        = md5( $code_twilio );
												$_SESSION['register_step']['code_twilio_expiry'] = time();
												// Use the client to do fun stuff like send text messages!
												$client->messages->create(
												// the number you'd like to send the message to
													$_POST['phone_number1'] . $_POST['phone_number2'] . $_POST['phone_number3'],
													[
														// A Twilio phone number you purchased at twilio.com/console
														'from' => $twilio_phone_number,
														// the body of the text message you'd like to send
														'body' => "" . $code_twilio . " is your verification code from Leede Jones Gable Inc."
													]
												);
												$getLastResponse                                  = $client->httpClient->lastResponse;
												$getContent                                       = $getLastResponse->getContent();
												$getBody                                          = $getContent['body'];
												$_SESSION['register_step']['submit_sms_twilio']   = 'success';
												$_SESSION['register_step']['phone_number1']       = trim( $_POST['phone_number1'] );
												$_SESSION['register_step']['phone_number2']       = trim( $_POST['phone_number2'] );
												$_SESSION['register_step']['phone_number3']       = trim( $_POST['phone_number3'] );
												$_SESSION['register_step']['verify_phone_number'] = 'waiting';
												$_SESSION['register_step']['count_sms']           = isset( $_SESSION['register_step']['count_sms'] ) ? $_SESSION['register_step']['count_sms'] + 1 : 1;
											} catch ( Exception $e ) {
												$_SESSION['register_step']['submit_sms_twilio'] = 'error';
												if ( isset( $_SESSION['register_step']['verify_phone_number'] ) ) {
													unset( $_SESSION['register_step']['verify_phone_number'] );
												}
											}
										}

									}
									$twilio_time       = ! empty( $leede_options['twilio_time'] ) ? (int) $leede_options['twilio_time'] : 300;
									$twilio_limit_sms  = ! empty( $leede_options['twilio_limit_sms'] ) ? (int) $leede_options['twilio_limit_sms'] : 3;
									$twilio_limit_code = ! empty( $leede_options['twilio_limit_code'] ) ? (int) $leede_options['twilio_limit_code'] : 3;
									$twilio_again      = ! empty( $leede_options['twilio_again'] ) ? (int) $leede_options['twilio_again'] : 3600;
									if ( isset( $_SESSION['register_step']['code_twilio_expiry'] ) && time() - $_SESSION['register_step']['code_twilio_expiry'] > $twilio_time ) {
										unset( $_SESSION['register_step']['code_twilio'] );
										unset( $_SESSION['register_step']['code_twilio_expiry'] );
									}
									if ( isset( $_POST['submit_code_twilio'] ) && ( ( isset( $_SESSION['register_step']['code_twilio'] ) && md5( $_POST['code_twilio'] ) != $_SESSION['register_step']['code_twilio'] ) || ! isset( $_SESSION['register_step']['code_twilio'] ) ) ) {
										$_SESSION['register_step']['count_code'] = isset( $_SESSION['register_step']['count_code'] ) ? $_SESSION['register_step']['count_code'] + 1 : 1;
									}
									if ( isset( $_POST['submit_code_twilio'] ) && isset( $_SESSION['register_step']['code_twilio'] ) && md5( $_POST['code_twilio'] ) == $_SESSION['register_step']['code_twilio'] && isset( $_SESSION['register_step']['count_sms'] ) && $_SESSION['register_step']['count_sms'] <= $twilio_limit_sms && isset( $_SESSION['register_step']['count_code'] ) && $_SESSION['register_step']['count_code'] <= $twilio_limit_code ) {
										$_SESSION['register_step']['verify_phone_number'] = 'sucessfully';
										unset( $_SESSION['register_step']['submit_sms_twilio'] );
										$_SESSION['register_step']['count_sms']  = 0;
										$_SESSION['register_step']['count_code'] = 0;
									}
									if ( ( isset( $_SESSION['register_step']['count_sms'] ) && $_SESSION['register_step']['count_sms'] > $twilio_limit_sms ) || ( isset( $_SESSION['register_step']['count_code'] ) && $_SESSION['register_step']['count_code'] > $twilio_limit_code ) ) {
										$_SESSION['register_step']['verify_phone_number'] = 'unsucessfully';
									}
									if ( ( isset( $_POST['submit_sms_twilio'] ) || isset( $_POST['submit_code_twilio'] ) ) && isset( $_SESSION['register_step']['verify_phone_number'] ) && $_SESSION['register_step']['verify_phone_number'] == 'unsucessfully' ) {
										$_SESSION['register_step']['verify_phone_number_expiry'] = time();
									}
									if ( isset( $_SESSION['register_step']['verify_phone_number_expiry'] ) && time() - $_SESSION['register_step']['verify_phone_number_expiry'] > $twilio_again ) {
										unset( $_SESSION['register_step']['submit_sms_twilio'] );
										$_SESSION['register_step']['count_sms']  = 0;
										$_SESSION['register_step']['count_code'] = 0;
										unset( $_SESSION['register_step']['verify_phone_number'] );
										unset( $_SESSION['register_step']['verify_phone_number_expiry'] );
									}
									?>
									<?php if ( isset( $_SESSION['register_step']['verify_phone_number'] ) && $_SESSION['register_step']['verify_phone_number'] == 'sucessfully' ) { ?>
                                        <div class="code-wrap">
                                            <p>
												<?php
												$cellphone = $_SESSION['register_step']['phone_number1'] . ' ' . $_SESSION['register_step']['phone_number2'] . ' ' . $_SESSION['register_step']['phone_number3'];
												echo leede_getCellphone( $cellphone );
												?>
                                                <a class="active-send-wrap send-wrap-link" href="javascript:void(0)">
													<?php echo esc_html__( 'Change Number', 'leede' ) ?>
                                                </a>
                                            </p>
											<?php
											//Successful Verification Description
											if ( $leede_options['register_step2_success_desc'] ) {
												echo do_shortcode( $leede_options['register_step2_success_desc'] );
											}
											?>
                                        </div>
									<?php } ?>
									<?php if ( isset( $_SESSION['register_step']['verify_phone_number'] ) && $_SESSION['register_step']['verify_phone_number'] == 'unsucessfully' ) { ?>
                                        <div class="code-wrap">
                                            <p>
												<?php
												$cellphone = $_SESSION['register_step']['phone_number1'] . ' ' . $_SESSION['register_step']['phone_number2'] . ' ' . $_SESSION['register_step']['phone_number3'];
												echo leede_getCellphone( $cellphone );
												?>
                                            </p>
											<?php
											//Unsuccessful Verification Description
											if ( $leede_options['register_step2_unsuccess_desc'] ) {
												echo do_shortcode( $leede_options['register_step2_unsuccess_desc'] );
											}
											?>
                                        </div>
									<?php } ?>
									<?php if ( isset( $_SESSION['register_step']['verify_phone_number'] ) && ( $_SESSION['register_step']['verify_phone_number'] ) == 'waiting' ) { ?>
                                        <div class="code-wrap">
                                            <p>
												<?php
												$cellphone = $_SESSION['register_step']['phone_number1'] . ' ' . $_SESSION['register_step']['phone_number2'] . ' ' . $_SESSION['register_step']['phone_number3'];
												echo leede_getCellphone( $cellphone );
												?>
                                                <a class="active-send-wrap send-wrap-link" href="javascript:void(0)">
													<?php echo esc_html__( 'Change Number', 'leede' ) ?>
                                                </a>
                                            </p>
											<?php
											//Verify Phone Number Form Description
											if ( $leede_options['register_step2_verify_desc'] ) {
												echo do_shortcode( $leede_options['register_step2_verify_desc'] );
											}
											?>
                                            &nbsp;<a href="javascript:void(0)" class="cellphone-btn other-code">
												<?php echo esc_html__( 'Send Another Code', 'leede' ); ?>
                                            </a>
                                            <br>
                                            <br>
                                            <div class="cellphone-code">
                                                <input title="code" type="text" name="code_twilio1" maxlength="7">
                                            </div>
                                            <p class="error error_code_twilio">
												<?php if ( isset( $_POST['submit_code_twilio'] ) && ( isset( $_SESSION['register_step']['code_twilio'] ) && md5( $_POST['code_twilio'] ) != $_SESSION['register_step']['code_twilio'] || ! isset( $_SESSION['register_step']['code_twilio'] ) ) ) { ?>
													<?php echo esc_html__( 'Sorry the verification code you entered in invalid. Please try again?', 'leede' ); ?>
												<?php } ?>
                                            </p>
                                            <a href="javascript:void(0)" class="code-btn button">
												<?php echo esc_html__( 'Verify This Code', 'leede' ) ?>
                                            </a>
                                        </div>
									<?php } ?>
                                    <div class="send-wrap">
										<?php
										//Send SMS Form Description
										if ( $leede_options['register_step2_send_desc'] ) {
											echo do_shortcode( $leede_options['register_step2_send_desc'] );
										}
										?>
                                        <div class="cellphone-number">
                                            (<input id="phone1" maxlength="3" class="phone-number" title="Cell phone"
                                                    type="text"
                                                    name="cellphone1"
                                                    value="<?php if ( isset( $_SESSION['register_step']['phone_number1'] ) ) {
												        echo $_SESSION['register_step']['phone_number1'];
											        } ?>">)
                                            <span></span>
                                            <input id="phone2" maxlength="3" class="phone-number" title="Cell phone"
                                                   type="text"
                                                   name="cellphone2"
                                                   value="<?php if ( isset( $_SESSION['register_step']['phone_number2'] ) ) {
												       echo $_SESSION['register_step']['phone_number2'];
											       } ?>">
                                            <span></span>
                                            <input id="phone3" maxlength="4" class="phone-number" title="Cell phone"
                                                   type="text"
                                                   name="cellphone3"
                                                   value="<?php if ( isset( $_SESSION['register_step']['phone_number3'] ) ) {
												       echo $_SESSION['register_step']['phone_number3'];
											       } ?>">
                                        </div>
                                        <p class="error error_phone_number"><?php if ( isset( $_SESSION['register_step']['submit_sms_twilio'] ) && $_SESSION['register_step']['submit_sms_twilio'] == 'error' ) { ?><?php echo esc_html__( "Something's wrong with your phone number.", 'leede' ) ?><?php } ?></p>
										<?php
										//Send SMS Form Note
										if ( $leede_options['register_step2_send_note'] ) {
											echo do_shortcode( $leede_options['register_step2_send_note'] );
										}
										?>
                                        <a href="javascript:void(0)" class="cellphone-btn button">
											<?php echo esc_html__( 'Send Code', 'leede' ); ?>
                                        </a>
                                    </div>
                                </div>
                                <input type="hidden" name="verify_phone_number"
                                       value="<?php if ( $_SESSION['register_step']['verify_phone_number'] ) {
									       echo $_SESSION['register_step']['verify_phone_number'];
								       } ?>">
                                <span class="error error_verify_phone_number"></span>
                            </div>
                        </fieldset>
                        <h3><?php echo esc_html__( 'Address', 'leede' ); ?></h3>
						<?php
						//Registration - Step 2 - Verify Account
						if ( $leede_options['register_step2_address_content'] ) {
							echo do_shortcode( $leede_options['register_step2_address_content'] );
						}
						?>
                        <fieldset>
                            <label for="address_lookup"
                                   class="label-pl"><?php echo esc_html__( 'Address Lookup', 'leede' ); ?>:</label>
                            <div class="field">
                                <input id="address_lookup" type="text" name="address_lookup"
                                       placeholder="<?php echo esc_attr__( 'Start Typing Your Address Here', 'leede' ); ?>">
                                <div style="margin-top: 10px;margin-bottom: 20px"><?php echo esc_html__( 'International clients, please enter your address below', 'leede' ) ?></div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label><span class="required">* </span><?php echo esc_html__( 'Address', 'leede' ); ?>:
                            </label>
                            <div class="field">
                                <input title="Address" type="text" name="address1"
                                       value="<?php echo isset( $_POST['hidden_address1'] ) ? $_POST['hidden_address1'] : ''; ?>">
                                <span class="error error_address1"></span>
                                <input title="Address" type="text" name="address2"
                                       value="<?php echo isset( $_POST['hidden_address2'] ) ? $_POST['hidden_address2'] : ''; ?>">
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="country">
                                <span class="required">* </span><?php echo esc_html__( 'Country', 'leede' ); ?>:
                            </label>
                            <div class="field">
								<?php
								$data_json  = file_get_contents( get_theme_file_uri( '/js/address.json' ) );
								$data_array = json_decode( $data_json, true );
								?>
                                <select id="country" name="country" data-json='<?php echo $data_json ?>'>
									<?php foreach ( $data_array as $value ) { ?>
                                        <option value="<?php echo $value['countryShortCode']; ?>"
											<?php
											if ( isset( $_POST['hidden_country'] ) && $_POST['hidden_country'] == $value['countryShortCode'] ) {
												echo 'selected';
											}
											if ( ( ! isset( $_POST['hidden_country'] ) || isset( $_POST['hidden_country'] ) && $_POST['hidden_country'] == '' ) && $value['countryShortCode'] == 'CA' ) {
												echo 'selected';
											}
											?>>
											<?php echo $value['countryName']; ?>
                                        </option>
									<?php } ?>
                                </select>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="province">
                                <span class="required">* </span><?php echo esc_html__( 'Province', 'leede' ); ?>:
                            </label>
                            <div class="field">
                                <select id="province" name="province"></select>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="city">
                                <span class="required">* </span>
								<?php echo esc_html__( 'City', 'leede' ); ?>:
                            </label>
                            <div class="field">
                                <input id="city" type="text" name="city"
                                       value="<?php echo isset( $_POST['hidden_city'] ) ? $_POST['hidden_city'] : ''; ?>">
                                <span class="error error_city"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="postal_code"><span
                                        class="required">* </span><?php echo esc_html__( 'Postal Code', 'leede' ); ?>:
                            </label>
                            <div class="field">
                                <input id="postal_code" type="text" name="postal_code"
                                       value="<?php echo isset( $_POST['hidden_postal_code'] ) ? $_POST['hidden_postal_code'] : ''; ?>">
                                <span class="error error_postal_code"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="advisor">
                                <span class="required">* </span>
								<?php echo esc_html__( 'Investment Advisor', 'leede' ); ?>:
                            </label>
                            <div class="field">
								<?php $advisors = leede_getAdvisors(); ?>
                                <select id="advisor" name="advisor">
                                    <option value=""><?php echo esc_html__( 'Select your Advisor ', 'leede' ); ?></option>
									<?php foreach ( $advisors as $k => $a ): ?>
                                        <option value="<?php echo $k; ?>" <?php if ( isset( $_POST['hidden_advisor'] ) && $_POST['hidden_advisor'] == $k ) {
											echo 'selected';
										}; ?>><?php echo $a; ?></option>
									<?php endforeach; ?>
                                </select>
                                <span class="error error_advisor"></span>
                            </div>
                        </fieldset>
                        <h3><?php echo esc_html__( 'Choose A Password', 'leede' ); ?></h3>
                        <fieldset>
                            <label class="label-pl"><em><?php echo esc_html__( 'Username', 'leede' ); ?>:</em></label>
                            <div class="field">
                                <em><?php echo $_SESSION['register_step']['username']; ?></em>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="password"><span
                                        class="required">* </span><?php echo esc_html__( 'Password', 'leede' ); ?>:
                            </label>
                            <div class="field">
                                <input id="password" type="password" name="password"
                                       value="<?php echo isset( $_POST['hidden_password'] ) ? $_POST['hidden_password'] : ''; ?>">
                                <span class="error error_password"></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <label for="confirm_password">
                                <span class="required">* </span><?php echo esc_html__( 'Confirm Password', 'leede' ); ?>
                                :
                            </label>
                            <div class="field">
                                <input id="confirm_password" type="password" name="confirm_password"
                                       value="<?php echo isset( $_POST['hidden_confirm_password'] ) ? $_POST['hidden_confirm_password'] : ''; ?>">
                                <span class="error error_confirm_password"></span>
                                <button type="submit" name="submit_register_step2">
									<?php echo esc_html__( 'Finish Registration', 'leede' ) ?>
                                </button>
                            </div>
                        </fieldset>
                    </form>
				<?php }
				if ( isset( $_SESSION['register_step']['send_security_code'] ) && $_SESSION['register_step']['send_security_code'] == 'expired' ) { ?>
                    <h1><?php echo esc_html__( 'Registration Continued', 'leede' ); ?></h1>
					<?php
					//Failed - Link in email expired
					if ( $leede_options['register_step1_expired_content'] ) {
						echo do_shortcode( $leede_options['register_step1_expired_content'] );
					}
					?>
				<?php }
				$email = isset( $_POST['username'] ) ? trim( $_POST['username'] ) : '';
				if ( $email != '' && ! username_exists( $email ) && ! email_exists( $email ) ) {
					//Email - To Client For Confirming Email Address
					$array                     = array(
						'username' => $email,
						'time'     => time(),
					);
					$_SESSION['register_step'] = $array;
					//Email - To Client For Confirming Email Address
					$send_security_code = leede_getTplPageURL( 'templates/page-register.php' ) . '?send_security_code=' . base64_encode( $_SESSION['register_step']['username'] . ' ' . $_SESSION['register_step']['time'] );
					$logo_img           = get_stylesheet_directory_uri() . '/images/logo.jpg';
					$to                 = $email;
					if ( $leede_options['register_step1_email_subject'] ) {
						$subject = $leede_options['register_step1_email_subject'];
					} else {
						$subject = 'Confirming Email Address';
					}
					if ( $leede_options['register_step1_email_body'] ) {
						$body = do_shortcode( $leede_options['register_step1_email_body'] );
					} else {
						$body = '
                        <p><img alt="img" src="' . $logo_img . '"/></p>
                        <h3>Please confirm Registration</h3>
                        <p>
                        Thank you for starting the registration process. In order to complete your registration please click the confirmation link below. After clicking this link you will be taken to a page where you can continue the next steps in the online registration process. This will include validating your identity, providing one or more of your account numbers, as well as validating your cell phone number
                        </p>
                        <p><a href="' . $send_security_code . '">' . $send_security_code . '</a><p>
                        <p>If you have problems to open this link, please copy and paste the above URL into your web browser.</p>
                        <p>If you received this email by mistakes, simply delete it.</p>
                        <p>If you have any questions please contact your investment advisor.</p>
                        <br>
                        <p><small>This is an automatically delivered message and replies to this address cannot be answered.</small></p> 
                        <h3 style="font-weight: normal; padding-top: 15px; border-top: 1px solid #eee">© Leede Jones Gable Inc. 2020</h3>
                        ';
					}
					if ( $leede_options['register_step1_email_body'] ) {
						$body .= '<br>' . do_shortcode( $leede_options['register_step1_email_signature'] );
					} else {
						$body .= '
						<br>
                        <p><small>This is an automatically delivered message and replies to this address cannot be answered.</small></p> 
                        <h3 style="font-weight: normal; padding-top: 15px; border-top: 1px solid #eee">© Leede Jones Gable Inc. 2020</h3>
						';
					}
					$headers = array( 'Content-Type: text/html; charset=UTF-8' );
					if ( $leede_options['register_step1_email_headers'] ) {
						$headers[] = $leede_options['register_step1_email_headers'];
					}
					wp_mail( $to, $subject, $body, $headers );
				}
				if ( isset( $_SESSION['register_step']['username'] ) && ! isset( $_SESSION['register_step']['send_security_code'] ) ) {
					?>
                    <h1><?php echo esc_html__( '2. Email Confirmation', 'leede' ); ?></h1>
					<?php
					//Thanks Page - Email Sent
					if ( $leede_options['register_step1_thanks_content'] ) {
						echo do_shortcode( $leede_options['register_step1_thanks_content'] );
					}
					?>
				<?php }
				if ( ! isset( $_SESSION['register_step']['username'] ) && ! isset( $_SESSION['register_step']['send_security_code'] ) ) { ?>
                    <form method="post" class="register_form register_step1" action="">
                        <h1><?php echo esc_html__( 'Register for the Client Dashboard', 'leede' ); ?></h1>
						<?php
						//Registration - Step 1 - Verify Email
						if ( $leede_options['register_step1_verify_email_content'] ) {
							echo do_shortcode( $leede_options['register_step1_verify_email_content'] );
						}
						?>
                        <fieldset>
                            <label for="username">
								<?php echo esc_html__( 'Email', 'leede' ); ?>:</label>
                            <div class="field">
                                <input id="username" type="email" name="username" value="<?php echo $email; ?>">
                                <span class="error error_username"></span>
                                <button type="submit"
                                        name="submit_register_step1"><?php echo esc_html__( 'Continue', 'leede' ); ?></button>
								<?php if ( username_exists( $email ) || email_exists( $email ) ) { ?>
                                    <div class="box">
										<?php
										//Error - Email Already Used
										if ( $leede_options['register_step1_error_content'] ) {
											echo do_shortcode( $leede_options['register_step1_error_content'] );
										}
										?>
                                    </div>
								<?php } ?>
                            </div>
                        </fieldset>
                    </form>
				<?php }
			} ?>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            function getProvince() {
                var dataJson = $('#country').attr('data-json');
                var dataArray = JSON.parse(dataJson);
                var country = $('#country').val();
                var province = '<?php echo isset( $_POST["hidden_province"] ) ? $_POST["hidden_province"] : "";?>';
                var option = '';
                for (i = 0; i < dataArray.length; i++) {
                    if (dataArray[i]['countryShortCode'] == country) {
                        for (j = 0; j < dataArray[i]['regions'].length; j++) {
                            var selected = '';
                            if (dataArray[i]['regions'][j]['shortCode'] != undefined) {
                                if (province == dataArray[i]['regions'][j]['shortCode']) {
                                    var selected = ' selected';
                                }
                                option += '<option value="' + dataArray[i]['regions'][j]['shortCode'] + '" ' + selected + '>' + dataArray[i]['regions'][j]['name'].replace("`", "'") + '</option>';
                            } else {
                                if (province == dataArray[i]['countryShortCode']) {
                                    var selected = ' selected';
                                }
                                option += '<option value="' + dataArray[i]['countryShortCode'] + '" ' + selected + '>' + dataArray[i]['regions'][j]['name'].replace("`", "'") + '</option>';
                            }
                        }
                        break;
                    }
                }
                jQuery('#province').empty().append(option);
            }

            if ($('#country').length > 0) {
                getProvince();
                $('#country').change(function () {
                    getProvince();
                });
            }
        });
    </script>
<?php
get_footer();