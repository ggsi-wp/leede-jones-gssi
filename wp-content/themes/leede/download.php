<?php
/**
 * Template name: Download
 * Page specifically for downloading documents from Doxim
 */
if( isset( $_GET['docid'] ) && is_user_logged_in() ) 
{   
    $str    = my_simple_crypt( $_GET['docid'], 'd' ); 
    $params = explode( '_', $str );
    $test   = "curl";
    $bin    = esc_attr( get_option('doxim_bin') );
    $passwd = esc_attr( get_option('doxim_password') );
    $apiUrl = esc_attr( get_option('doxim_apiurl') ); 

    $mid    = $params[1]; //filter_var( $_GET['mid'], FILTER_SANITIZE_NUMBER_INT );
    $docid  = $params[2]; //filter_var( $_GET['docid'], FILTER_SANITIZE_NUMBER_INT );
    $viewid = $params[0]; //filter_var( $_GET['viewid'], FILTER_SANITIZE_NUMBER_INT ); 
    //$fileLink   = $apiUrl . "bin=" . $bin . "&passwd=" . $passwd . "&viewid=" . $viewid . "&mid=" . $mid . "&docid=" . $docid; //var_dump( $fileLink );
    $vars   = "bin=$bin&passwd=$passwd&viewid=$viewid&mid=$mid&docid=$docid"; //var_dump( $fileLink );
    // "https://edocs.leedefinancial.com/api/getDocuments?bin=8341355181&passwd=bcx90igo&viewid=75&mid=0230001&docid=1198926";

    switch( $test )
    {
        case 'iframe':
            echo "<iframe src='" . $fileLink . "' width='100%' height='100%'></iframe>";
            break;
        case 'curl':


            $filename   = $viewid . '-' . $mid . '-' . $docid . '.pdf';


            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, "https://edocs.leedefinancial.com/api/getDocuments?$vars" );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array( "Content-Type: application/pdf" ) );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
            //curl_setopt( $ch, CURLOPT_REFERER, $fileLink );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE ); 
            curl_setopt( $ch, CURLOPT_CAINFO, getcwd() . "../../plugins/lj-doxim/DigiCertGlobalRootCA.crt" );   
            //curl_setopt($ch, CURLOPT_VERBOSE, 1);    
            
            $data = curl_exec($ch);
            //var_dump( $data );

            curl_close($ch);

            header('Content-type: application/pdf');
            echo $data;

            
        //    $result = file_put_contents( "C:\\Windows\\Temp\\" . $filename, $data );
            
        /*        if(!$result){
                        echo "error";
                }else{
                        echo "success";
                }
            break;*/
    }

        //header('Content-Type: application/pdf');
    // header('Content-Length: ' . filesize($fileLink));
        //header("Content-Description: File Transfer"); 
    // header( 'Content-disposition: attachment; filename=test.pdf' ); 

    // readfile($fileLink);
    // exit();
}
else
{
    echo '<p>Document not found.</p>';
}

?>

